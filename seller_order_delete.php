<?php

session_start();

include("db_login.php");

//insert the boxes of choice into the database
if(array_key_exists("order", $_POST) && isSellerActive($db) && canSell($db)) {
    //echo(print_r($_POST));
    
    
    //make queries to the db depending on what select menu chosed from
    if(array_key_exists("art_nr", $_POST) && array_key_exists("quantity", $_POST) && $_POST["art_nr"] != "") {
    //make a query to get the name of the box using the box_art_nr
        echo("trying to add the box");
        
        $query = getTitleQuery($db, $_POST["art_nr"]);
        $boxTitle = getBoxTitle($db, $query);
        
        //echo($boxTitle);
        
        $query = getInsertQuery($db, $boxTitle, $_POST["art_nr"], $_POST["quantity"]);
    
        //echo($query);
    
        if(!mysqli_query($db, $query)) {
            echo("fail");
        } else {
            //send a success response to the ajax request   
            echo("success");
        }
        
    } else {
        echo("success");
    }
    
    //delete an order

}


function getTitleQuery($db, $article_nr) {
    return "SELECT `title` FROM `boxes` WHERE `box_art_nr` = '".mysqli_real_escape_string($db, $article_nr)."' LIMIT 1";
}

function getInsertQuery($db, $boxTitle, $box_art_number, $quantity) {
    //first test to see if the box exists in sellers list
    $testQuery = "SELECT `nr_of_boxes` FROM `sellers_list` WHERE `sellers_list`.`seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `box_art_nr` = '".mysqli_real_escape_string($db, $box_art_number)."' AND `isActive` = 1 LIMIT 1";
    $testRes = mysqli_query($db, $testQuery);
    $testRow = mysqli_fetch_array($testRes);
    
    if($testRow["nr_of_boxes"] > 0 && ($quantity > 0 && $quantity < 11)) {
        //if box exists just update the ammount of that specific box
        return "UPDATE  `mixboxen_se_mixboxen_boxes`.`sellers_list` SET  `nr_of_boxes` = `nr_of_boxes` + '".$quantity."' WHERE  `sellers_list`.`seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `sellers_list`.`box_art_nr` = '".mysqli_real_escape_string($db, $box_art_number)."' AND `isActive` = 1;";
    
    } else if($quantity > 0 && $quantity < 11) {
        //the box does not exist so we have to insert all the information into the list
        return "INSERT INTO `mixboxen_se_mixboxen_boxes`.`sellers_list` (`id`, `seller_id`, `nr_of_boxes`, `mix_box`, `box_art_nr`, `buyer`, `isActive`, `date`) VALUES (NULL, '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."', '".$quantity."', '".$boxTitle."', '".mysqli_real_escape_string($db, $box_art_number)."', '".mysqli_real_escape_string($db, $_POST["buyer_name"])."', '1', '".mysqli_real_escape_string($db, getTimeAndDate())."');";
    }
}


function getDeleteQuery($db, $id) {
    return  "DELETE FROM `mixboxen_se_mixboxen_boxes`.`sellers_list` WHERE `id` = '".mysqli_real_escape_string($db, $id)."' LIMIT 1";
}

function getBoxTitle($db, $query) { 
    $result = mysqli_query($db, $query);
    $row = mysqli_fetch_array($result);
    return $row[0];
}

function getTimeAndDate() {
    $timeAndDate = date("Y-m-d H:i:sa");
    return $timeAndDate;
}

function isSellerActive($db) {
    //there is a session ID, let's check so that the user hasn't been deleted while still logged in
    $isActiveQuery = "SELECT * FROM `seller` WHERE `seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `isActive` = '1' LIMIT 1";
    
    $isActiveResult = mysqli_query($db, $isActiveQuery);
    $isActiveRow = mysqli_fetch_array($isActiveResult);
    
   //echo(sizeof($isActiveRow));
    
    if(sizeof($isActiveRow) > 0) {
        return true;
        //echo("true");
    } else {
        //echo("false");
        return false;
        
    }
}

function canSell($db) {
    // get the canSell boolean from the db where seller id = session.
    $canSellQuery = "SELECT `canSell` FROM `seller` WHERE `seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `isActive` = '1' LIMIT 1";
    
    $canSellRes = mysqli_query($db, $canSellQuery);
    $canSellRow = mysqli_fetch_array($canSellRes);
    
   //echo(sizeof($isActiveRow))
    
    if($canSellRow[0] == 1) {
        return true;
        //echo("true");
    } else {
        //echo("false");
        return false;
        
    }
}



?>