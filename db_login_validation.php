<?php

session_start();

include("db_login.php");

if(isset($_POST["login"])) {
    
    //echo(print_r($_POST));
    
    $account_type = $_POST["radio"];
    $first_name = mysqli_real_escape_string($db, $_POST["first_name"]);
    $email = mysqli_real_escape_string($db, $_POST["email"]);
    $password = mysqli_real_escape_string($db, $_POST["password"]);
    //trim the password, email both left and right
    $password = ltrim(rtrim($password));
    $email = ltrim(rtrim($email));
    
    if($account_type == 'seller') {
       //echo("steeek");    
    //get the password from the db so we can compare the passwords for the login process    
        $loginQuery = "SELECT `password`, `seller_id` FROM `seller` WHERE `email` = '".$email."' AND `first_name` = '".$first_name."' AND `isActive` = 1 LIMIT 1";
        
        $result = mysqli_query($db, $loginQuery);
        $row = mysqli_fetch_array($result);
        
        //test the 3 login fields
        if(isset($row)) {
            //test the password
            if($row["password"] == $password) {
                //the seller is logged in. Now let's set the session variables
                //seller_id because we need to separate the seller and the contact at login
                $_SESSION["seller_id"] = $row["seller_id"];
//                //see if user wants to be remembered with a cookie.
//                if($remember_me == 1) {
//                    
//                    setcookie("seller_cookie", (111+(int)$row["seller_id"]), time() + 60*60*24*7); //remember user for 7 days
//                }
                //redirect the seller to his/her control panel
                //header("Location: seller_control_panel.php");
                echo("seller");
                //echo('<div class="container">Seller <strong>'.$first_name.'</strong> logged in successfully</div>');
                
            } else {
                echo("<div class='alert alert-danger offset-md-3 col-md-6' role='alert'><strong>Ooops!</strong> Fel lösenord.</div>");
            }
            
        } else {
            echo("<div class='alert alert-danger offset-md-3 col-md-6' role='alert'>Du skrev fel på någon av uppgifterna!<br />Försök igen!</div>");
        }

        //end of seller login
        //contact login
    } else {
        
        //query for validating the contact
        $loginQuery = "SELECT `password`, `contact_id` FROM `contact` WHERE `email` = '".$email."' AND `isActive` = 1 LIMIT 1";
        
        $result = mysqli_query($db, $loginQuery);
        $row = mysqli_fetch_array($result);
            
        //check if the email is valid or if the contact is actually active
        if(isset($row)) {
            //the user is registered and active, now let's check that password and set some variables
                
            if($row["password"] == $password) {
                //the user is granted access. Now set the session variables (maybe use the contact id)
                $_SESSION["contact_id"] = $row["contact_id"];
//                //set the cookie if remeber me is == 1
//                if($_POST["remember_me"] == 1) {
//                    setcookie("contact_cookie", (111+(int)$row["contact_id"]), time() + 60*60*24*7);
//                }
                //redirect the contact to the control panel
                //header("Location: contact_control_panel.php");
                echo("contact");
                //the row below is unreachable 
//                echo("<div class='container'>Välkommen <strong>".$first_name."</strong> till din egen mixbox sida.</div>");
            } else {
                echo("<div class='alert alert-danger offset-md-3 col-md-6' role='alert'><strong>Ooops!</strong> Fel lösenord.</div>");
            }
        } else {
            echo("<div class='alert alert-danger offset-md-3 col-md-6' role='alert'>Felaktig e-post adress!</div>");
         
        }    
    } //end of contact login
}

mysqli_close($db);
?>

