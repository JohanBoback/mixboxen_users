<?php

session_start();

if(!isset($_SESSION["admin_id"])) {
    header("Location: admin_login.php");
}

include("db_login.php");

$contactLink = "";

$tooltipDeleteContact = "Är du säker på att du vill ta bort kontaktpersonen? Kom ihåg att alla säljare som hör till kontakten också tas bort inklusive säljarnas transaktioner.";

if(array_key_exists("response", $_GET)) {
    $contactLink = $_GET["response"];
}

//build the table of contact persons
$query = "SELECT `first_name`, `last_name`, `email`, `phone_number`, `association`, `contact_id`, `hasOrdered` FROM `contact` WHERE `isActive` = 1 ORDER BY `contact_id`";

$rowArray = mysqli_fetch_all(mysqli_query($db, $query));

$contactTable = "";
//loop through the array[][] of arrays each one containing one row from the db 
for($i = 0; $i < sizeof($rowArray); $i++) {
    if($rowArray[$i][6] == 1) {
        $contactTable.= "<tr class='red' style='font-style:italic' name=".$rowArray[$i][5]."><td>".$rowArray[$i][0]."</td><td>".$rowArray[$i][1]."</td><td>".$rowArray[$i][2]."</td><td>".$rowArray[$i][3]."</td><td id='association'>".$rowArray[$i][4]."</td><td id='id'>".$rowArray[$i][5]."</td></tr>";
    } else {
        $contactTable.= "<tr name=".$rowArray[$i][5]."><td>".$rowArray[$i][0]."</td><td>".$rowArray[$i][1]."</td><td>".$rowArray[$i][2]."</td><td>".$rowArray[$i][3]."</td><td id='association'>".$rowArray[$i][4]."</td><td id='id'>".$rowArray[$i][5]."</td></tr>";
    }
}

mysqli_close($db);

?>

<html>
    <head>
            <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
         <!--refresh every 60s-->     
    <!--    <meta http-equiv="refresh" content="30">-->

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="bootstrap.min.css">
        <script src="https://use.fontawesome.com/34eb041fe3.js"></script>
        <style type="text/css">
            
            input {
                margin-top: 1%;
            }
            
            .gradient {
              background: linear-gradient(white, #B6B6B6);
            }
            
            .red {
                color: firebrick;
            }
        
            #code {
                width: 100%;
                font-size: 1.2em;
            }
            
            .container {
                text-align: center;
            }
       
            .five-down {
                margin-top: 5%;
            }
            
            .ten-down {
                margin-bottom: 10%;
            }
            
            .center {
                text-align: center;
            }
            
            table {
                margin-top: 7%;
            }
            
            .opacity {
                opacity: 1;
            }
            
            #header-ids > th:hover {
                color: skyblue;
                cursor:pointer;
            }
            
            #delete-contact {
                font-size: 0.9em !important;
            }
            
            tr:hover {
                cursor:pointer;
            }
            
            #logout, #change-password-btn {
                margin-top: 1%;
            }
            
            #change-password-btn {
                padding: 10px;
            }
            
                        /* apple colors */
            [class*='-dark'] {
                background-color:#02243C;
                color:#f0f0f0;
            }

            [class*='-blue'] {
                background-color:#5195ce;
                color:#fff;
                
            }
            [class*='-apple'] {
                background-color:#005DB3;
                color:#f8f8f8;
       

        </style>
    </head>
    <body class="gradient">
        <a href="admin_login.php?logout=1"><button id="logout" type="button" class="btn btn-secondary">LOGOUT</button></a>
        <a href="admin_change_password.php"><button id="change-password-btn" type="button" class="btn btn-secondary"><i class="fa fa-cog" aria-hidden="true"></i></button></a>
        <div class="container col-md-8 offset-md-2 five-down">
            <div class="row ten-down">
                <div class="col-md-4">
                    <form id="form-generate" class="form-inline" method="POST" action="admin_start_backend.php">
                        <button id="reload" type="button" class="btn btn-outline-primary">Uppdatera kontaktlista</button>
                    </form>
                </div> 
                <div id="form-delete-div" class="offset-md-2 col-md-6">
                    <form id="form-delete" class="form-inline" method="POST" action="admin_start_backend.php">
                        <div class="form-group">
                            <input name="contact_id" type="text" class="form-control" id="contact-id" placeholder="Kontakt ID" required>
<!--
                        </div>
                        <div class="form-group">
-->
                            <input id="delete-contact" name="delete_contact" type="submit" class="btn btn-outline-danger" value="Ta bort kontakt" data-toggle="tooltip" data-placement="bottom" title="<?php echo($tooltipDeleteContact); ?>">
                        </div>
                    </form>
                </div>    
            </div>    
            <div class="row ten-down">
                <div id="link-div"><?php echo($contactLink) ?></div>
            </div>
        </div>
        <div id="dialog-div"></div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="table-responsive">
                    <table id="contacts-table" class="table table-striped">
                        <thead class="thead alert-blue">
                            <th colspan="6"><button id="generate-contact" type="button" name="generate_contact" class="btn btn-secondary col-md-4 offset-md-4 opacity">Genera länk till ny kontaktperson</button></th>
                            <tr id="header-ids">
                                <th id="1">Förnamn</th>
                                <th id="2">Efternamn</th>
                                <th id="3">E-mail</th>
                                <th id="4">Telefonnummer</th>
                                <th id="5">Förening/Lag/Klass</th>
                                <th id="6">ID</th>
                            </tr>
                        </thead>
                        <tbody id="ajax_response">
                            <?php echo($contactTable) ?>
                        </tbody>    
                    </table>
                    </div>    
                </div>    
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="table-responsive">
                    <table id="box-table" class="table table-striped">
                        <thead class="thead alert-blue">
                            <th id="sell-status-header" colspan="3" class="center"></th>
                            <tr class="second-header">
                                <th>Antal</th>
                                <th>Namn</th>
                                <th>Artikelnummer</th>
                            </tr>
                        </thead>
                        <tbody id="sales">
                            
                        </tbody>
                    </table>
                    </div>
                </div>    
            </div>
            
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="jquery.min.js"></script>
        <script src="tether.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <script type="text/javascript">
  
            //prevent the form to be sumbitted by pressing the enter key
            $(document).ready(function() {
                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                    }
                });
            });
            
            bindTrs();
        
            $('#generate-contact').click(function(){
                $('#link-div').text('');
                $.ajax({
                    type: "POST",
                    url: "admin_start_backend.php",
                    data: {generate_contact: true},
                    
                    success: function(data) {
                        $('#link-div').html(data);
                    }
                });
            });
            
            $('input').on('input', function(event){
                    liveValidation(event.target.name);
            });
            
            function liveValidation(name) {
                console.log(name);
            }
            
            $('#form-delete').submit(function(event){
                
                var del = confirm("Är du säker på att du vill ta bort kontaktpersonen? Kom ihåg att alla säljare som hör till kontakten också tas bort inklusive säljarnas transaktioner.");
                
                $('#link-div').text('');
                
                if(del) {
                    $('#delete-contact').trigger("click");
                } else {
                    event.preventDefault();
                }
                
                
            });
            
            var thValue = "6";
            
            $('#header-ids > th').bind('click', function(){
                thValue = this.id;
                $.ajax({
                    type: "POST",
                    url: "admin_sort_table.php",
                    data: { content: this.id },
                    
                    
                    success: function(data){
                        $('#ajax_response').html(data);
                        bindTrs();
                    }
                });
            });
           
            $('#reload').click(function(){
                        $('#link-div').text('');
                        $('#contact_id').val('');
                        window.location.replace("admin_start_page.php");
            });
           
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
            
            function bindTrs() {
                
                
                $('#ajax_response > tr').bind('click', function(){
                $('#sell-status-header').text('Säljstatus för ' + $(this)[0].children[4].innerHTML);
                    //console.log($(this)[0].children[4].innerHTML);
                    $.ajax({
                        type: "POST",
                        url: "get_contact_sales.php",
                        data: {
                           contact_id: $(this).attr('name') 
                        },
                        
                        success: function(data){
                            $('#sales').html(data);
                        }
                    });
                });
            }
            
            
            
        </script>
    </body>
</html>