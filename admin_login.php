<?php

session_start();

include("db_login.php");

if(isset($_GET["logout"])) {
    session_unset();
}

if(isset($_POST["submit"])) {
    $query = "SELECT `id`, `first_name` FROM `mixboxen_admins` WHERE `email` = '".mysqli_real_escape_string($db, $_POST['email'])."' AND `password` = '".mysqli_real_escape_string($db, $_POST["password"])."' LIMIT 1;";
    $result = mysqli_query($db, $query);
    $row = mysqli_fetch_array($result);
    
    
    
    if(sizeof($row) > 0) {
        $_SESSION["admin_id"] = $row[0];
        $_SESSION["first_name"] = $row[1];
        header("Location: admin_start_page.php");
    } else {
        echo("<div class='alert alert-danger center'>Either a \"typo\" or you're not allowed here!</div>");
    }
}

mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
      
      <style type="text/css">
          
          
          .gradient {
              background: linear-gradient(white, #B6B6B6);
          }
      
          form {
              margin-top: 20%;
          }
          
          .center {
              text-align: center;
          }
        
      </style>
      
  </head>
  <body class="">
    <div class="container">
      <form class="form-inline offset-md-3 col-md-7" method="POST">
          <input type="text" class="form-control" name="email" id="email" placeholder="email">
          <input type="password" class="form-control" name="password" id="password" placeholder="password">
          <input id="submit" name="submit" type="submit" class="btn btn-secondary" value="LOGIN">
      </form>
    </div>      

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" integrity="VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>
  </body>
    
    <script type="text/javascript">
   
    
    
    </script>
    
</html>