<?php

include("db_login.php");

//generate a pw to add to url plus insert into db for validation
$generatedString = getLoginString(16);

//generate url link with password and insert the pw into db for validation later on.
$httpLink = "http://www.mixboxen.se/users/contact_registration.php?login=".$generatedString;
    echo('<div class="container"><input type="text" id="code" value="'.$httpLink.'" autofocus></div>');

$query = "INSERT INTO `mixboxen_se_mixboxen_boxes`.`contact_signup_passwords` (`id`, `temp_passwords`) VALUES ('NULL', '".mysqli_real_escape_string($db, $generatedString)."');";

//echo($query);

if(mysqli_query($db, $query)) {
    echo("<div class='container'>Signup password successfully registered!<br />Send the link above to your contact person</div>");
} else {
    echo("<div class='container'><h2 style='color:red;'>DATABASE FAILURE!!!<br />Please try again.</h2></div>");
}


//pw generator taking one arg of pw length
function getLoginString($nrOfChars) {
    
    $alpha = "abcdefghijklmnopqrstuvwxyz";
    $alpha_upper = strtoupper($alpha);
    $numeric = "0123456789";
    $special = ".-+=_,!@$#*%<>[]{}";
    $chars = "";


    $chars = $alpha.$alpha_upper.$numeric;
    $length = $nrOfChars;


    $len = strlen($chars);
    $pw = '';

    for ($i=0;$i<$length;$i++)
            $pw .= substr($chars, rand(0, $len-1), 1);

    // the finished password
    $pw = str_shuffle($pw);

    return $pw;
}

?>



<html>
    <head>
        <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
        
        <style type="text/css">
        
            #code {
                width: 100%;
                font-size: 1.2em;
            }
            
            .container {
                text-align: center;
                margin-top: 3%;
            }
            
            #password {
                margin-bottom: 15%;
            }
            
            .ten-down {
                margin-top: 10%;
            }
            
        
        </style>
    </head>
    <body>
        <div class="container ten-down">
            <a href="signup_generator.php"><button type="button" class="btn btn-primary">Generate new login URL</button></a>
        </div>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    </body>
</html>