<?php

session_start();

include("db_login.php");

//print_r($_COOKIE);

//if there's no session key redirect the user back to the login page
if(!array_key_exists("contact_id", $_SESSION)) {
    header("Location: index.php");
} else {
    
    if(!isSellerActive($db)) {
        session_unset();
        header("Location: is_active_error.php");
    }
}

$query = "SELECT `first_name`, `last_name`, `association`, `contact_id` FROM `contact` WHERE `contact_id` = '".mysqli_real_escape_string($db, $_SESSION['contact_id'])."' AND `isActive` = 1 LIMIT 1";

$result = mysqli_query($db, $query);
$row = mysqli_fetch_array($result);


$contact_last_name = $row["last_name"];
$contact_first_name = $row["first_name"];
$firstName = rtrim(ltrim($contact_first_name));
$firstName = substr($firstName, 0, 2);
$firstNameArray = str_split($firstName);

$toFoolPeopleHash = rand(10000, 99999);
//end of create contact_id

//generate link for sellers
$httpLink = "http://www.mixboxen.se/users/seller_registration.php?checksum=".$firstNameArray[1].rand(200, 999).$row[contact_id].rand(200, 999).$firstNameArray[0]."&rnd=".$toFoolPeopleHash;
//end of generate link



function isSellerActive($db) {
    //there is a session ID, let's check so that the user hasn't been deleted while still logged in
    $isActiveQuery = "SELECT * FROM `contact` WHERE `contact_id` = '".mysqli_real_escape_string($db, $_SESSION["contact_id"])."' AND `isActive` = '1' LIMIT 1";
    
    $isActiveResult = mysqli_query($db, $isActiveQuery);
    $isActiveRow = mysqli_fetch_array($isActiveResult);
    
   //echo(sizeof($isActiveRow));
    
    if(sizeof($isActiveRow) > 0) {
        return true;
        //echo("true");
    } else {
        //echo("false");
        return false;
        
    }
}


?>

<html>
    <head>
        <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--//refreshes the page every 20sek-->    
<!--    <meta http-equiv="refresh" content="2">-->

    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="jquery-ui.min.css">
        <link rel="stylesheet" href="jquery-ui.theme.min.css">
        <link rel="stylesheet" href="jquery-ui.structure.min.css">
        <style type="text/css">
            
            .gradient {
              background: linear-gradient(white, #d6d6d6);
            }

        
            #code {
                width: 90%;
                font-size: 1.0em;
                margin-bottom: 2%;
                margin-top: 0px;
            }
            
            #logout-btn {
                float:right;
                margin-top: 1%;
                margin-bottom: 2%;
            }
            
            .container-fluid {
                text-align: center;
            }
            
            h5 {
                margin-bottom: 0px;
            }
            
            #sellers-table {
                margin-top: 10%;
                
            }
            
            #cont-panel-header {
                font-size: 1.6em;
                padding: 5%;
            }
            
            #password {
                margin-bottom: 15%;
            }
            
            .ten-down {
                margin-top: 10%;
            }
            
            #box-table th {
                color: #EAAA4F;
            }
            
            .center {
                text-align: center;
            }
            
            #order-btn {
                margin-top: 5%;
                margin-bottom: 10%;
            }
            
            #order-success {
                margin-top: 3%;
            }
            
            .green {
                color:limegreen;
            }
            
            .moneydiv {
                font-size: 1.2em;
                margin-top: 2%;
            }
        
                
        </style>
    </head>
    <body class="gradient">
        
        <div class="row">
            <div class="col-md-2 offset-md-1">
                <!--a href="contact_control_panel.php"><button type="button" class="btn btn-success" id="refresh-btn">Uppdatera försäljningar</button></a-->
            </div>
            <div class="col-md-2 offset-md-7" id="logout-btn">
                <a href="index.php?logout=contact"><button type="button" class="btn btn-secondary">Logga ut mig</button></a>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="offset-md-2 col-md-8 offset-md-2">
                    <div id="cont-panel-header" class="col-md-12 alert alert-success">Välkommen <strong><?php echo($row["first_name"]); ?></strong> till din personliga mixbox-sida.<br />Här ser du vad och hur mycket dina säljare har sålt.</div>
                </div>     
            </div>
            <div class="row">
                <div id="total" class="offset-md-3 col-md-2 moneydiv alert alert-info">SÅLDA BOXAR:<br /><strong><span id="total-boxes"></span></strong> st</div>
                <div id="kickback" class="col-md-2 offset-md-2 moneydiv alert alert-info">NI HAR TJÄNAT:<br /><span class="green"><strong><span id="total-money"></span></strong></span> kr</div>
            </div>
            
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="table-responsive">
                    <table id="sellers-table" class="table table-striped">
                        <thead class="thead alert alert-info">
                            <th colspan="6" class="center">Säljare anslutna till <?php echo($contact_first_name." ".$contact_last_name); ?></th>
                            <tr class="second-header">
                                <th>Förnamn</th>
                                <th>Efternamn</th>
                                <th>E-mail</th>
                                <th>Telefon</th>
                                <th>ID</th>
                                <th>AntalBoxar</th>
                            </tr>
                        </thead>
                        <tbody id="sellers-table-out">
                            
                        </tbody>    
                    </table>
                    </div>
                </div>    
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="table-responsive">
                    <table id="box-table" class="table table-striped">
                        <thead class="thead alert alert-warning">
                            <th colspan="4" class="center">Boxar att beställa</th>
                            <tr class="second-header">
                                <th>Antal</th>
                                <th>Namn</th>
                                <th>Artikelnummer</th>
                                <th>ID</th>
                            </tr>
                        </thead>
                        <tbody id="boxes-table-out">
                        
                        </tbody>
                    </table>
                    </div>
                </div>    
            </div>
            <div class="row">
                <div class="offset-md-4 col-md-4 offset-md-4">
                    <button id="order-btn" type="button" class="btn btn-warning">Beställ ovanstående lista</button>
                </div>    
            </div>
            <div class="row">
                <div id="order-success"></div>
            </div>
            <div id="order-div" class="row ten-down alert alert-info offset-md-1 col-md-10 offset-md-1">
                <div><h5>Länken som era säljare skall använda för att registrera sig:</h5><br/><input type="text" id="code" value='<?php echo($httpLink); ?>'></div>
            </div>
        
        </div>
        
        <div id="dialog" title="Bekräftelse på beställning" >
            Är du säker på att du vill skicka beställningen till Mixboxen?
        </div>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="jquery.min.js"></script>
        <script src="tether.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <script src="jquery-ui.min.js"></script>
        <script type="text/javascript">
        
        //jquery ui function for a ok cancel modal.    
            $(document).ready(function() {
                createSellersTable();
                createBoxesTable();
                $("#dialog").dialog({
                  autoOpen: false,
                  modal: true
                });
              });

              $("#order-btn").click(function(e) {
                e.preventDefault();
                //var targetUrl = $(this).attr("href");

                $("#dialog").dialog({
                  buttons : {
                    "Beställ" : function() {
                      //console.log("beställ")
                      //console.log(getTableData($('#box-table')));
                        
                        $.ajax({
                            type: "POST",
                            url: "order_handler.php",
                            data: {
                                tableArray: getTableData($('#box-table')),
                                hasOrdered: true
                            },
                            
                            success: function(data) {
                                $('#order-success').html(data);
                            }
                        });
                       $(this).dialog("close"); 
                    },
                    "Avbryt" : function() {
                      $(this).dialog("close");
                    }
                  }
                });

                $("#dialog").dialog("open");
              });
            
            
            
        
        //refresh the page every half minute
//            setInterval(function(){
//                location.reload();
//            }, 30000);
        
            //console.log(document.getElementById('box-table').rows[2].cells[1].innerHTML);
        
        //function for passing a table and getting the th/td values as an array
            function getTableData(table) {
                var data = [];
                table.find('tr').each(function (rowIndex, r) {
                    var cols = [];
                    $(this).find('th,td').each(function (colIndex, c) {
                        cols.push(c.textContent);
                    });
                data.push(cols);
                });
            return data;
            }
            
            setInterval(function(){
                createSellersTable();
            }, 15000);
            
            setInterval(function(){
                createBoxesTable();
            }, 10000);
            
            function createSellersTable() {
                $.ajax({
                    type: 'POST',
                    url: 'contact_control_panel_backend.php',
                    data: {
                        contact: <?php echo($_SESSION['contact_id']) ?>
                    },
                        
                        success: function(data) {
                            //console.log(data);
                            $('#sellers-table-out').html(data);
                        }
                });
            }
                       
            function createBoxesTable() {
                $.ajax({
                    type: 'POST',
                    url: 'contact_control_panel_backend.php',
                    data: {
                        contact: <?php echo($_SESSION['contact_id']) ?>,
                        boxes: 1 
                    },
                        
                        success: function(data) {
                            var jsonOut = $.parseJSON(data);
                            var totalBoxes = parseInt(jsonOut[1]);
                            $('#boxes-table-out').html(jsonOut[0]);
                            $('#total-boxes').text(totalBoxes);
                            $('#total-money').text(50*totalBoxes);
                        }
                });
            }
            
            
            
        </script>        
    </body>
</html>