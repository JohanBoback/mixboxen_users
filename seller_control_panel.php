<?php


session_start();

include("db_login.php");

//create the select-option
$selectOption = "";

$query = "SELECT `box_art_nr`, `title` FROM `boxes` ORDER BY `id`";
$result = mysqli_query($db, $query);
$rowsArray = mysqli_fetch_all($result);

for($i = 0; $i < sizeof($rowsArray); $i++) {
    $selectOption.="<option value='".$rowsArray[$i][0]."'>".$rowsArray[$i][1]."</option>";
    if(($i+1)%9 == 0) {
        $selectOption.="<optgroup label='-----------------------'></optgroup>";
    }
}

$deleteInstructions = "För att ta bort en box: Välj boxen i listan och tryck 'Ta bort'. För att ta bort ett helt ID med flera boxar: Mata in ID numret och tryck 'Ta bort";
$addInstructions = "Välj box och antal";


//if there's no session key redirect the user back to the login page
if(!array_key_exists("seller_id", $_SESSION)) {
    header("Location: index.php");
} else {
    
    if(!isSellerActive($db)) {
        session_unset();
        header("Location: is_active_error.php");
    }
}

//get the user details and store them into an array
$query = "SELECT `first_name`, `last_name`, `email`, `contact_id` FROM `seller` WHERE `seller_id` = ".mysqli_real_escape_string($db, $_SESSION["seller_id"])." AND `isActive` = 1 LIMIT 1";
$result = mysqli_query($db, $query);
$row = mysqli_fetch_array($result);
//get the contact person details to output to the bottom of the seller screen
$contactPersonQuery = "SELECT `first_name`, `last_name`, `phone_number`, `email` FROM `contact` WHERE `contact_id` = '".$row[3]."' AND `isActive` = '1' LIMIT 1";
$contactResult = mysqli_query($db, $contactPersonQuery);
$contactRow = mysqli_fetch_array($contactResult);

$contactPerson = "<div>Min kontaktperson: <strong>".$contactRow[0]." ".$contactRow[1]."</strong><br />E-mail: <strong>".$contactRow[3]."</strong><br />Telefon: <strong>".$contactRow[2]."</strong>";

//lets get some stuff from the database regarding sold objects
$soldQuery = "SELECT `nr_of_boxes`, `mix_box`, `box_art_nr`, `date`, `id` FROM `sellers_list` WHERE `seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `isActive` = 1";

$result = mysqli_query($db, $soldQuery);
//convert the result to a php multidimensional array
$rowsArray = mysqli_fetch_all($result);

$boxesTable = "";
$canSellString = "";
$sumBoxes = 0;

for($i = 0; $i < sizeof($rowsArray); $i++) {
    $sumBoxes += $rowsArray[$i][0];
    $boxesTable.="<tr><td>".$rowsArray[$i][0]."</td><td>".$rowsArray[$i][1]."</td><td>".$rowsArray[$i][2]."</td><td>".$rowsArray[$i][3]."</td><td>".$rowsArray[$i][4]."</td></tr>";
}
//end of rendering the box table

$boxTitle = "";

if(array_key_exists("delete", $_POST) && isSellerActive($db) && canSell($db)) {
    //delete by making an exact copy of an order but pressing the delete button instead
    if(isset($_POST["art_nr"]) && $_POST["id"] == "") {
        //first ask the db if there is only one box left. 
        //If so delete the box, otherwise just modify the nr_of_boxes.
        $quantityLeftQuery = "SELECT `nr_of_boxes`, `id` FROM `sellers_list` WHERE `seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `box_art_nr` = '".mysqli_real_escape_string($db, $_POST["art_nr"])."' LIMIT 1;";
        //echo($quantityLeftQuery);
        
        if($result = mysqli_query($db, $quantityLeftQuery)) {
            $row = mysqli_fetch_array($result);
            //if there is > 1 box let nr_of_boxes -= 1 else delete the box from the list
            if($row[0] > 1) {
                $query = "UPDATE `sellers_list` SET `nr_of_boxes` = `nr_of_boxes` - 1 WHERE `id` = '".$row[1]."';";
                //echo($query);
                if(!mysqli_query($db, $query)) {
                    $canSellString = "<div class='alert alert-danger'>Någonting blev fel vid borttagning av objekt.<br />Försök igen. Om inte problemet löser sig, kontakta mixboxen.se.</div>";
                } else {
                    //delete successful
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            } else { //delete by id
                $query = getDeleteQuery($db, $row[1]);
                if(!mysqli_query($db, $query)) {
                    $canSellString = "<div class='alert alert-danger'>Någonting blev fel vid borttagning av objekt.<br />Försök igen. Om inte problemet löser sig, kontakta mixboxen.se.</div>";
                } else {
                    //delete successful
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            }
        }
        
        
        
        
    } else if($_POST["id"] != "") { //if delete by id
        $query = getDeleteQuery($db, $_POST["id"]);

        if(!mysqli_query($db, $query)) {
            $canSellString = "<div class='alert alert-danger'>Någonting blev fel vid borttagning av objekt.<br />Försök igen. Om inte problemet löser sig, kontakta mixboxen.se.</div>";
        } else {
            //delete successful
            header('Location: '.$_SERVER['REQUEST_URI']);
        }
    } //end of delete by id 
} else if(!canSell($db)) {
    $canSellString = "<div class='alert alert-danger'>Försäljningen är nu avslutad!<br />Mixboxen tackar så mycket för din medverkan.</div>";
}

mysqli_close($db);

function getTitleQuery($db, $article_nr) {
    return "SELECT `title` FROM `boxes` WHERE `box_art_nr` = '".mysqli_real_escape_string($db, $article_nr)."' LIMIT 1";
}

function getInsertQuery($db, $boxTitle, $box_art_number, $quantity) {
    //first test to see if the box exists in sellers list
    $testQuery = "SELECT `nr_of_boxes` FROM `sellers_list` WHERE `sellers_list`.`seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `box_art_nr` = '".mysqli_real_escape_string($db, $box_art_number)."' AND `isActive` = 1 LIMIT 1";
    $testRes = mysqli_query($db, $testQuery);
    $testRow = mysqli_fetch_array($testRes);
    
    if($testRow["nr_of_boxes"] > 0 && ($quantity > 0 && $quantity < 11)) {
        //if box exists just update the ammount of that specific box
        return "UPDATE  `mixboxen_se_mixboxen_boxes`.`sellers_list` SET  `nr_of_boxes` = `nr_of_boxes` + '".$quantity."' WHERE  `sellers_list`.`seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `sellers_list`.`box_art_nr` = '".mysqli_real_escape_string($db, $box_art_number)."' AND `isActive` = 1;";
    
    } else if($quantity > 0 && $quantity < 11) {
        //the box does not exist so we have to insert all the information into the list
        return "INSERT INTO `mixboxen_se_mixboxen_boxes`.`sellers_list` (`id`, `seller_id`, `nr_of_boxes`, `mix_box`, `box_art_nr`, `buyer`, `isActive`, `date`) VALUES (NULL, '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."', '".$quantity."', '".$boxTitle."', '".mysqli_real_escape_string($db, $box_art_number)."', '".mysqli_real_escape_string($db, $_POST["buyer_name"])."', '1', '".mysqli_real_escape_string($db, getTimeAndDate())."');";
    }
}


function getDeleteQuery($db, $id) {
    return  "DELETE FROM `mixboxen_se_mixboxen_boxes`.`sellers_list` WHERE `id` = '".mysqli_real_escape_string($db, $id)."' LIMIT 1";
}

function getBoxTitle($db, $query) { 
    $result = mysqli_query($db, $query);
    $row = mysqli_fetch_array($result);
    return $row[0];
}

function getTimeAndDate() {
    $timeAndDate = date("Y-m-d H:i:sa");
    return $timeAndDate;
}

function isSellerActive($db) {
    //there is a session ID, let's check so that the user hasn't been deleted while still logged in
    $isActiveQuery = "SELECT * FROM `seller` WHERE `seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `isActive` = '1' LIMIT 1";
    
    $isActiveResult = mysqli_query($db, $isActiveQuery);
    $isActiveRow = mysqli_fetch_array($isActiveResult);
    
   //echo(sizeof($isActiveRow));
    
    if(sizeof($isActiveRow) > 0) {
        return true;
        //echo("true");
    } else {
        //echo("false");
        return false;
        
    }
}

function canSell($db) {
    // get the canSell boolean from the db where seller id = session.
    $canSellQuery = "SELECT `canSell` FROM `seller` WHERE `seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `isActive` = '1' LIMIT 1";
    
    $canSellRes = mysqli_query($db, $canSellQuery);
    $canSellRow = mysqli_fetch_array($canSellRes);
    
   //echo(sizeof($isActiveRow))
    
    if($canSellRow[0] == 1) {
        return true;
        //echo("true");
    } else {
        //echo("false");
        return false;
        
    }
}



?>

<html>
    <head>
        <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<!--
     refresh every 20s     
    <meta http-equiv="refresh" content="20">
-->

   <!-- Bootstrap CSS -->    
    <link rel="stylesheet" href="bootstrap.min.css">
        
        <style type="text/css">
            
            .gradient {
              background: linear-gradient(#e6e6e6, #a6a6a6);
            }

            #code {
                width: 70%;
                font-size: 1.0em;
                margin-bottom: 2%;
                margin-top: 0px;
            }
            
            #logout-btn {
                float:right;
                margin-top: 10px;
                margin-bottom: 5px;
                margin-right: 7px;
            }
            

            .container-fluid {
                text-align: center;
            }
            

            #cont-panel-header {
                font-size: 1.6em;
                margin-bottom: 3%;
                margin-top: 10%;
            }
            
            .ten-down {
                margin-top: 10%;
            }
             
            .center {
                text-align: center;
            }
            
            button {
                font-size: 0.9em !important;
            }
            
            select {
                font-size: 0.9em !important;
                color: grey;
            }
            
            #select-boxes {
                margin-top: 2%;
                margin-bottom: 10%;
            }
            
  
            label {
                color: #548754;
            }
            
            #quantity {
                width: 60px;
                padding: 10 11 8 11;
            }
            
            .second-header > th:hover {
                color: dodgerblue;
                cursor: pointer;
            }
            
            #select-drop {
                width: 280px;
                margin-right: 2px;
                float: left;
            }
            
            #order-btn {
                float: right;
            }
            
            .green {
                color: limegreen;
            }
            
            #id {
                float:right;
                width: 100px;
            }
            
            #delete {
                float: right;
                margin-left: 7px;
                padding-top: 10px;
                padding-bottom: 9px;
            }
            
            input {
                margin-bottom: 4px;
            }
            
/*
            #box-table thead, #box-table th{
                background-color: #bFf0b8;
            }
*/
      
        </style>
    </head>
    <body class="gradient">
        <div class="row">
            <div class="col-md-2 offset-md-1">
                <!--a href="contact_control_panel.php"><button type="button" class="btn btn-success" id="refresh-btn">Uppdatera försäljningar</button></a-->
            </div>
            <div class="col-md-2 offset-md-7" id="logout-btn">
                <a href="index.php?logout=seller"><button type="button" class="btn btn-secondary">Logout</button></a>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="offset-md-2 col-md-8 offset-md-2">
                    <div id="cont-panel-header" class="col-md-12 alert alert-success">Välkommen <strong><?php echo($row["first_name"]); ?></strong> till din egen mixbox-bokningssida!<br />Här lägger du till bokade boxar för beställning</div>
                </div>     
            </div>
            <div class="row">
                <div id="can-sell" class=""><?php echo($canSellString); ?></div>
            </div>
            <div class="row">
                <div id="total" class="offset-md-3 col-md-2 moneydiv alert alert-success">SÅLDA BOXAR:<br /><strong><span id="total-boxes"><?php echo($sumBoxes) ?></span></strong> st</div>
                <div id="kickback" class="col-md-2 offset-md-2 moneydiv alert alert-info">DU HAR TJÄNAT:<br /><strong><span class="green" id="earnings"><?php echo($sumBoxes*50) ?></span></strong> kr</div>
            </div>
        </div>
        <div class="container-fluid">
            <form id="select-boxes" class="form-inline offset-md-3 col-md-6" method="post" action="">
                <div class="row">
                    <select id="select-drop" name="art_nr" class="form-control">
                        <option disabled="disabled" selected="selected">VÄLJ EN MIXBOX</option>
                        <?php echo($selectOption) ?>
                    </select>
                    <!--input id="buyer-name" name="buyer_name" type="text" class="form-control" placeholder="Köpare (inte nödvändigt)"-->
                    <input id="quantity" class="form-control" type="number" name="quantity" value="1" min=1 max=10 required>
                    <label for="quantity">Antal boxar</label>
                    <button id="order-btn" name="order" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="right" title="<?php echo($addInstructions); ?>">Lägg till box</button>
                </div>
            </form>
                <div class="row">
                    <div class="offset-md-5 col-md-6"> 
                        <button name="delete" form="select-boxes" id="delete" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="<?php echo($deleteInstructions); ?>">Ta bort box/ID</button>
                        <input id="id" form="select-boxes "class="form-control" type="number" name="id" placeholder="ID">    
                    </div>    
                </div>           
        </div>
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="table-responsive">
                <table id="box-table" class="table table-striped">
                    <thead class="thead alert-info">
                        <th colspan="5" class="center alert-info">Dina bokade mixboxar.</th>
                        <tr class="second-header">
                            <th id="1">Antal</th>
                            <th id="2">Namn</th>
                            <th id="3">Artikelnummer</th>
<!--                            <th id="4">Köpare</th>-->
                            <th id="5">Orderdatum</th>
                            <th id="6">ID</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php echo($boxesTable) ?>
                    </tbody>    
                </table>
                </div>    
            </div>    
        </div>
        <div class="row">
            <div class="offset-md-7 col-md-4 offset-md-1">
            
                <div id="contact-person" class="alert alert-warning ten-down"><?php echo($contactPerson); ?></div>
            
            </div>
        </div>
      
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script> 
        
        <script type="text/javascript">
            //prevent the form to be sumbitted by pressing the enter key
            $(document).ready(function() {
                $('#select-boxes').on('submit', function(event){
                    event.preventDefault();
                });
                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                    }
                });
            });
            
            //passes the event triggered by select to the resetSelects function.
            $("select").change(function(event){
                resetSelects(this.value);
                event.target.style.backgroundColor = "#DFF0D8";
                event.target.style.color = "#4A7641";
                //set the this (select) . children (option) to the value of kermit. You can pass a js variable to css //selectors by using the + sign as shown below.
                $(this).children("option[value="+kermit+"]").prop('selected',true)
                
            });
            
            //create a variable that can be reached from both functions
            var kermit = "";
            
            //here the value passed will be set to kermit and used in the prop selected in the other function
            function resetSelects(value) {
                $("select").prop("selectedIndex", 0);
                $("select").css("background-color", "#FFFFFF");
                $("select").css("color", "grey");
                $("#id").val("");
                document.getElementById("quantity").value = 1;
                kermit = value;
            }
            
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
            
//            var thValue = "2";
            
            //updates the table sorting it by th value
            $('.second-header > th').bind('click', function(event){
                thValue = this.id;
                updateSellerTable(thValue);
//                console.log(thValue);
            });
            
            //function to update the sellerTable by sending a sort value to the sort table backend.
            function updateSellerTable(sortValue) {
                $.ajax({
                    type: "POST",
                    url: "seller_control_panel_backend.php",
                    data: { content: sortValue },
                    
                    success: function(data){
                        var jsonOut = $.parseJSON(data);
                        var totalBoxes = parseInt(jsonOut[1]);
                        //console.log(jsonOut);
                        $('tbody').html(jsonOut[0]);
                        $('#total-boxes').text(totalBoxes);
                        $('#earnings').text(totalBoxes*50);
                        
                    }
                });
            }
            
            $('#order-btn').click(function(event){
                $.ajax({
                    type: "POST",
                    url: "seller_order_delete.php",
                    data: {
                        order: true,
                        art_nr: $('#select-drop').val(),
                        quantity: $('#quantity').val()
                    },
                    success: function(data) {
                        updateSellerTable("2");
                        resetSelects(0);
                    }
                });
            });
            
            $('#delete').click(function(event){
                $.ajax({
                    type: "POST",
                    
                    data: {
                        delete: true,
                        art_nr: $('#select-drop').val(),
                        quantity: $('#quantity').val(),
                        id: $('#id').val()
                    },
                    success: function(data) {
                        updateSellerTable("2");
                        resetSelects(0);
                    }
                });
            });
        
        </script>
    </body>
</html>