<?php

session_start();

if($_GET["logout"] == "seller") {
    //The user has logged out. End the session and set the cookie to a time in the past.
    session_unset();
    //setcookie("seller_cookie", "", time() - 60*60);
    //$_COOKIE["seller_cookie"] = ""; //set the cookie to an empty string if it should be accessed later on
    
} else if(array_key_exists("seller_id", $_SESSION)) {
    header("Location: seller_control_panel.php");
} 

if($_GET["logout"] == "contact") {
    session_unset();
    //setcookie("contact_cookie", "", time() -60*60);
    //$_COOKIE["contact_cookie"] = "";
} else if(array_key_exists("contact_id", $_SESSION)) {
    header("Location: contact_control_panel.php");
}

?>



<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Mixboxen User Login</title>
   <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
      
      <style type="text/css">
        
          .gradient {
              background: linear-gradient(white, lightgrey);
          }
            
          .container {
            width: 100%;
            margin: auto;
            text-align: center;
          }
          
          h2 {
            margin-top: 4%;
            margin-bottom: 8%;
          }
          
          #radios {
            margin-bottom: 4% !important;    
          }
          
          
          #lost-password-div {
              margin-top: 2%;
          }
          
          button {
            
            background-color: #cc733d !important;
            border-color: #cc733d !important;
          }
          
          button:hover {
            background-color: #BB5E31 !important;
        }
          
          #lost-password-form input {
              
              margin-bottom: 4px;
          }
          
          #lost-password-btn {
              opacity: 0.7;
          }
          
          #out-div {
              margin-top: 5%;
          }
          
/*
          #rem-chckbx {
            margin-left: 10px;
            margin-right: 5px;
          }
*/

      </style>
      
  </head>
  <body>
      <div class="container gradient">
          
          <h2>MIXBOXEN --> Login för användare</h2>
           
        <form id="form" class="form-inline">
            <div id="radios">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="radio" id="radio1" value="seller" checked="true">
                    Jag är säljare
                  </label>
                </div>
                <div class="form-check">
                  <label id="right-radio" class="form-check-label">
                    <input class="form-check-input" type="radio" name="radio" id="radio2" value="contact">
                    Jag är kontaktperson
                  </label>
                </div>
            </div>
            <div id="name-input" class="form-group">
                <input name="first_name" type="text" class="form-control" id="first-name" placeholder="Förnamn på säljare" autofocus required>
            </div>
            <div class="form-group">
                <input name="email" type="email" class="form-control" id="email" placeholder="E-post adress" autofocus required>
            </div>
            <div class="form-group">
                <input name="password" type="password" class="form-control" id="password" placeholder="Lösenord" required>
            </div>
            <div class="form-group">
                <button id="submit-form-btn" name="login" type="button" class="btn btn-success">Logga in</button>
            </div>    
        </form>
          <div id="out-div"></div>
          <div id="lost-password-div" class="row alert alert-info offset-md-4 col-md-4" hidden=true>
            <h6>Glömt lösenord?</h6>
            <p style="font-size: 0.7em">Välj med knapparna om du är säljare eller kontaktperson<br /></p>
              <form method="POST" class="form-inline" id="lost-password-form" action="lost_password.php">
                <input id="lost-password-input" name="email" type="email" class="form-control-sm" placeholder="registrerad e-mail" required>
                <div class="row">
                    <input id="seller-first-name" type="text" class="form-control-sm" placeholder="säljarens förnamn">  
                </div>  
                <input id="lost-password-btn" type="button" type="button" class="form-control-sm btn btn-primary" value="Skicka lösenord">
              </form>
          </div>
      </div>
    
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
      
      <script type="text/javascript">
          
          var nrOfFailedLogins = 0;
          
          //prevent the form to be sumbitted by pressing the enter key
            $(document).ready(function() {
                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                    }
                });
            });
          
          $('#submit-form-btn').on('click', function(event){
              
              if(nrOfFailedLogins >= 2) {
                  $('#lost-password-div').prop('hidden', false);
              }
              
              $.ajax({
                  type: "POST",
                  url: "db_login_validation.php",
                  data: {
                    radio: $('input[name="radio"]:checked').val(),
                    first_name: $('#first-name').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    login: "login"  
                  },
                  
                  success: function(data) {
        //to compare the ajax return have to trim the returning data since it can contain invisible line spaces etc
                      if($.trim(data) == "seller") {
                          window.location = "seller_control_panel.php";
                      } else if($.trim(data) == "contact") {
                          window.location = "contact_control_panel.php";
                      } else {
                          nrOfFailedLogins++;
                          $('#out-div').html(data);    
                      }
                  }
              });
          });
          
          $('#lost-password-btn').on('click' ,function(event){
            
              $.ajax({
                  type: "POST",
                  url: "lost_password.php",
                  data: {
                      email: $('#lost-password-input').val(),
                      typeOfPerson: $('#radio1').is(':checked'),
                      firstName: $('#seller-first-name').val()
                  },
                  
                  success: function(data){
                      //console.log(data);
                      $('#lost-password-input').val('');
                      $('#seller-first-name').val("");
                      $('#out-div').html(data);
                      $('#lost-password-div').prop('hidden', true);
                      nrOfFailedLogins = 0;
                  }
              });
          });

        $('#radio2').click(function(){
            clearAllInputs();
            $('#first-name').remove();
        });
          
        $('#radio1').click(function(){
            clearAllInputs();
            if(!$('#first-name').length) { //check to se if the node exists or not
                $('#name-input').append("<input name='first_name' type='text' class='form-control' id='first-name' placeholder='Förnamn på säljare' autofocus required>");
            }
        });
          
        function clearAllInputs() {
            $('#first-name').val("");
            $('#email').val("");
            $('#password').val("");
            $('#out-div').text("");
            $('#lost-password-btn').attr('value', 'Skicka lösenord');
        }
      
      </script>
  </body>
</html>