<?php

session_start();

include("db_login.php");

$phpOut = "";

//get the logged-in user's password

$nameQuery = "SELECT `first_name` FROM `mixboxen_admins` WHERE `id` = '".$_SESSION["admin_id"]."' LIMIT 1";
$result = mysqli_query($db, $nameQuery);
$row = mysqli_fetch_array($result);

$adminFirstName = $row[0];

if(isset($_POST["change-admin-password"])) {
//    echo(print_r($_POST));
    if(strcmp($_POST["password"], $_POST["password-repeat"]) != 0) {
        $phpOut = "<div class='alert alert-danger center'>Du har skrivit fel i något av fälten.<br />Försök igen!</div>";
    } else {
        $passwordQuery = "UPDATE `mixboxen_admins` SET `password` = '".mysqli_real_escape_string($db, $_POST["password"])."' WHERE `id` = '".$_SESSION["admin_id"]."' LIMIT 1;";
        if(mysqli_query($db, $passwordQuery)) {
            $phpOut = "<div class='alert alert-info center'>Lösenordet ändrat!<br />Tryck på pilen för att logga in igen!</div>";
            session_unset();
        } else {
            $phpOut = "<div class='alert alert-danger center'>Något sket sig!<br />Prova igen.</div>";
        }
    }
}

mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/34eb041fe3.js"></script>  
      <style type="text/css">
      
          .gradient {
              background: linear-gradient(white, #B6B6B6);
          }
      
          form {
              margin-top: 8%;
          }
          
          .center {
              text-align: center;
          }
          
          .container {
              margin-top: 2%;
              width: 100%;
              text-align: center;
          }
          
          input {
              margin-top: 1%;
          }
          
          #admin-header {
              color: orange;
          }
          
          i {
              margin-left: 2%;
          }
          
          h3 {
              padding-bottom: 2%;
          }
          
      </style>
      
  </head>
  <body>
    <div class="container">
        <div class="row">
            <h3 id="admin-header">Inställningar för <?php echo($adminFirstName) ?></h3>
            
        </div>
        <div id="out-div"><?php echo($phpOut); ?></div>
        
      <form class="form-inline offset-md-1 col-md-10 offset-md-1" method="POST">
          <a href="admin_start_page.php"><h3><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></h3></a>
          <input type="password" class="form-control" name="password" id="password" placeholder="password">
          <input type="password" class="form-control" name="password-repeat" id="password-repeat" placeholder="password-repeat">
          <input id="submit" name="change-admin-password" type="submit" class="btn btn-outline-warning" value="Change Password">
      </form>
    </div>      

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" integrity="VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>
  </body>
    
    <script type="text/javascript">
   
    //prevent the form to be sumbitted by pressing the enter key
        $(document).ready(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
    
    </script>
    
</html>