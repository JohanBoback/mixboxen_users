<?php

include("db_login.php");

$contactLink = "";

if(array_key_exists("generate_contact", $_POST)) {
    
    //print_r($_POST);
    
    //generate a pw to add to url plus insert into db for validation
    $generatedString = getLoginString(16);
    

    //generate url link with password and insert the pw into db for validation later on.
    $httpLink = "http://www.mixboxen.se/users/contact_registration.php?login=".$generatedString;
        $contactLink = '<div class="container"><input type="text" id="code" value="'.$httpLink.'" autofocus></div><br />';

    $query = "INSERT INTO `mixboxen_se_mixboxen_boxes`.`contact_signup_passwords` (`id`, `temp_passwords`, `date`) VALUES ('NULL', '".mysqli_real_escape_string($db, $generatedString)."', '".mysqli_real_escape_string($db, getTimeAndDate())."');";

    //echo($query);

    if(mysqli_query($db, $query)) {
        $contactLink.="<div class='container'>Temporärt inloggningslösenord skapat!<br />Skicka länken ovan till kontaktpersonen</div>";
        //header("Location: admin_start_page.php?response=".$contactLink);
    } else {
        $contactLink.="<div class='container'><h2 style='color:red;'>DATABASE FAILURE!!!<br />Please try again.</h2></div>";
    }
    
 echo($contactLink);
    
} else if(array_key_exists("delete_contact", $_POST)) {
    
    //check to see if the contact really exists
    $query = "SELECT * FROM `contact` WHERE `contact_id` = '".mysqli_real_escape_string($db, $_POST["contact_id"])."' AND `isActive` = '1' LIMIT 1";
    $result = mysqli_query($db, $query);
    $row = mysqli_fetch_array($result);
    //get the temp_login_password from the contact so that we can erase it from the table of temp_login_pwds
    $pwToDelete = $row[11];
    $contact_id = $row[0];
    
    //print_r($row);
    //if row array is > 0 the contact exists so remove it
    if(sizeof($row) > 0) {
        
        $query = "UPDATE `contact` SET `isActive` = '0', `date` = '".mysqli_real_escape_string($db, getTimeAndDate())."' WHERE `contact_id` = ".mysqli_real_escape_string($db, $_POST["contact_id"]);
        
        $delPwQuery = "DELETE FROM `contact_signup_passwords` WHERE `temp_passwords` = '".mysqli_real_escape_string($db, $pwToDelete)."' LIMIT 1";
        
        //we also have to delete the sellers connected to the contact via the contact_id
        $delContactSellers = "UPDATE `seller` SET `isActive` = '0', `date` = '".mysqli_real_escape_string($db, getTimeAndDate())."' WHERE `contact_id` = ".mysqli_real_escape_string($db, $_POST["contact_id"]);   
         
        if(mysqli_query($db, $query)) {
            $contactLink.="<div class='container'><h3 style='color:red;'>Contact nr: ".$_POST["contact_id"]." and all his/her sellers were removed.</h3></div>";
            //delete the temp password in the db.
            mysqli_query($db, $delPwQuery);
            
            deleteSellersBoxes($db, $_POST["contact_id"]);
            
            mysqli_query($db, $delContactSellers);
            
            header("Location: admin_start_page.php?response=".$contactLink);

        } else {
            $contactLink.="<div class='container'><h3 style='color:red;'>Something went wrong, please try again!</h3></div>";
            header("Location: admin_start_page.php?resoponse".$contactLink);
        }
        
    } else {
        header("Location: admin_start_page.php");
    }
    
mysqli_close($db);
}

function deleteSellersBoxes($db, $contact_id) {
    //find the sellers connected to the contact
    $findAllSellersQuery = "SELECT `seller_id` FROM `seller` WHERE `contact_id` = '".$contact_id."' AND `isActive` = 1";
    $findSellersRes = mysqli_query($db, $findAllSellersQuery);
    $rowsOfSellers = mysqli_fetch_all($findSellersRes);
    
    foreach($rowsOfSellers as $arrayOfIds) {
        $query = "UPDATE `sellers_list` SET `isActive` = 0 WHERE `seller_id` = '".$arrayOfIds[0]."'";
        mysqli_query($db, $query);
    }
}



//pw generator taking one arg of pw length
function getLoginString($nrOfChars) {
    
    $alpha = "abcdefghijklmnopqrstuvwxyz";
    $alpha_upper = strtoupper($alpha);
    $numeric = "0123456789";
    $special = ".-+=_,!@$#*%<>[]{}";
    $chars = "";


    $chars = $alpha.$alpha_upper.$numeric;
    $length = $nrOfChars;


    $len = strlen($chars);
    $pw = '';

    for ($i=0;$i<$length;$i++)
            $pw .= substr($chars, rand(0, $len-1), 1);

    // the finished password
    $pw = str_shuffle($pw);

    return $pw;
}

function getTimeAndDate() {
    $timeAndDate = date("Y-m-d H:i:sa");
    return $timeAndDate;
}


?>