<?php

include("db_login.php");


if(isset($_POST["contact"])) {
        //create the table with seller data

    //create queries for getting the table data
    //first find all the sellers by contact_id and get their seller_ids

    $queryOne = "SELECT `seller_id`, `first_name`, `last_name`, `email`, `phone_nr` FROM `seller` WHERE `contact_id` = ".mysqli_real_escape_string($db, $_POST["contact"]." AND `isActive` = 1 ORDER BY `seller_id`");
    $result = mysqli_query($db, $queryOne);

    $rows = mysqli_fetch_all($result);

    //print_r($rows);

    //table with all the sellers connected to the contact person
    $sellersTable = "";

    for($i = 0; $i < sizeof($rows); $i++) {

        $boxquery = "SELECT SUM(`nr_of_boxes`) FROM `sellers_list` WHERE `seller_id` = '".$rows[$i][0]."' AND `isActive` = 1";
        $boxres = mysqli_query($db, $boxquery);
        $boxrow = mysqli_fetch_array($boxres);

        $sellersTable.="<tr><td>".$rows[$i][1]."</td><td>".$rows[$i][2]."</td><td>".$rows[$i][3]."</td><td>".$rows[$i][4]."</td><td>".$rows[$i][0]."</td><td style='color:limegreen'>".$boxrow[0]."</td></tr>";
    }
    
    

    //table with all the boxes ordered by sellers
    $boxesTable = "";
    $totalSum = 0;

    for($j = 0; $j < sizeof($rows); $j++) {
        //get one query / seller
        $query = "SELECT `nr_of_boxes`, `mix_box`, `box_art_nr`, `seller_id` FROM `sellers_list` WHERE `seller_id` = ".mysqli_real_escape_string($db, $rows[$j][0])." AND `isActive` = 1 ORDER BY `nr_of_boxes`";
        $result = mysqli_query($db, $query);
        $rowArrays = mysqli_fetch_all($result);

        for($i = 0; $i < sizeof($rowArrays); $i++) {
            $totalSum+=(int)$rowArrays[$i][0];
            $boxesTable.="<tr><td>".$rowArrays[$i][0]."</td><td>".$rowArrays[$i][1]."</td><td>".$rowArrays[$i][2]."</td><td>".$rowArrays[$i][3]."</td></tr>";
        }   
    }
    
    if(isset($_POST["boxes"])) {
        echo(json_encode(array($boxesTable, $totalSum)));
    } else {
        echo($sellersTable);
    }
}




?>