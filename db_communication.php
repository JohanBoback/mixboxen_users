<?php

session_start();

include("db_login.php");

if(isset($_POST["submit_contact"])) {

    //array to store the post variables in
    $va = array();
        
    $first_name = mysqli_real_escape_string($db, $_POST["first_name"]);
    $last_name = mysqli_real_escape_string($db, $_POST["last_name"]);
    $street_address = mysqli_real_escape_string($db, $_POST["street_address"]);
    $zip_code = mysqli_real_escape_string($db, $_POST["zip_code"]);
    $city = mysqli_real_escape_string($db, $_POST["city"]);
    $email = filter_var(mysqli_real_escape_string($db, $_POST["email"]), FILTER_SANITIZE_EMAIL);
    $phone_number = mysqli_real_escape_string($db, $_POST["phone_number"]);
    $association = mysqli_real_escape_string($db, $_POST["association"]);
    $signup_password = mysqli_real_escape_string($db, $_POST["signup_password"]);
    
    
    // if isValid register the contact person
    if(isValidatedContact($first_name, $last_name, $street_address, $zip_code, $city, $email, $phone_number, $association)) {
    
         //generate random password
        include("password_generator.php");
        //end of pw generator


        $query = "INSERT INTO `mixboxen_se_mixboxen_boxes`.`contact` (`contact_id`, `first_name`, `last_name`, `street_address`, `zip_code`, `city`, `email`, `phone_number`, `association`, `password`, `isActive`, `signup_password`, `date`) VALUES (NULL, '".$first_name."', '".$last_name."', '".$street_address."', '".$zip_code."', '".$city."', '".$email."', '".$phone_number."', '".$association."', '".$pw."', '1', '".$signup_password."', '".mysqli_real_escape_string($db, getTimeAndDate())."')";

        //echo($query);

        //check if user mail already exists
        $checkQuery = "SELECT `email` FROM `contact` WHERE `email`= '".$email."' AND `isActive` = 1 LIMIT 1";
        $result = mysqli_query($db, $checkQuery);
        $row = mysqli_fetch_array($result);

        if(mysqli_num_rows($result) > 0) {
            echo("<div class='container'><strong>E-post adressen finns redan!</strong><br />Gå tillbaka och försök igen.</div>");
        } else {
            //try to insert contact into db
            if(!mysqli_query($db, $query)) {
                echo("<div class='container'>User not registered due to a network error.</div>");
            }

            //create a contact_id
            $query = "SELECT `contact_id` FROM `contact` WHERE `email` = '".mysqli_real_escape_string($db, $_POST['email'])."' AND `isActive` = 1 LIMIT 1";
            $result = mysqli_query($db, $query);
            $row = mysqli_fetch_array($result);

            $firstName = $_POST["first_name"];
            $firstName = rtrim(ltrim($firstName));
            $firstName = substr($firstName, 0, 2);
            $firstNameArray = str_split($firstName);

            $toFoolPeopleHash = rand(10000, 99999);
            //end of create contact_id

            //generate link for sellers
            $httpLink = "http://www.mixboxen.se/users/seller_registration.php?checksum=".$firstNameArray[1].rand(200, 999).$row[0].rand(200, 999).$firstNameArray[0]."&rnd=".$toFoolPeopleHash;

            echo('<div class="container"><div id="password" style="color:blue;">Ditt eget lösenord är: <h2 class="password">'.$pw.'</h2></div>
                    <h3>Skicka länken nedan till era säljare och be dem registrera sig via den.</h3>
                    <br/>
                    <input type="text" id="code" value="'.$httpLink.'" autofocus>
                </div>');
            //end of generate link
        }
    } else {
        echo("<div class='alert alert-danger container'>Du har skrivit fel på någon av uppgifterna!<br />Kom ihåg att alla fälten måste vara ifyllda.<br />Gå tillbaka och försök igen!");
    }
    
} else if(isset($_POST["submit_seller"])) {
    
    $contact_id = (int)mysqli_real_escape_string($db, $_POST["contact_id"]);
    $first_name = mysqli_real_escape_string($db, $_POST["first_name"]);
    $last_name = mysqli_real_escape_string($db, $_POST["last_name"]);
    $phone_number = mysqli_real_escape_string($db, $_POST["phone_number"]);
    $email = mysqli_real_escape_string($db, $_POST["email"]);
    
    //pass the variables to the function for validation
    
    if(isValidated($first_name, $last_name, $phone_number, $email)) {
    
        //generate random password
        include("password_generator.php");
        //end of pw generator

        $query = "INSERT INTO `mixboxen_se_mixboxen_boxes`.`seller` (`seller_id`, `contact_id`, `first_name`, `last_name`, `email`, `phone_nr`, `password`, `isActive`, `date`) VALUES (NULL, '".$contact_id."', '".$first_name."', '".$last_name."', '".$email."', '".$phone_number."', '".$pw."', '1', '".mysqli_real_escape_string($db, getTimeAndDate())."')";

        //check if seller exists in db -- more than one user / email allowed since "parents"
        $checkQuery = "SELECT `email`, `first_name`, `last_name` FROM `seller` WHERE `email` = '".$email."' AND `first_name` = '".$first_name."' AND `last_name` = '".$last_name."' AND `isActive` = 1 LIMIT 1";

        $result = mysqli_query($db, $checkQuery);

        if(mysqli_num_rows($result) > 0) {
            echo("<div class='alert alert-danger container'>Säljaren finns redan!</br >Gå tillbaka och försök igen!");
        } else {

            //double check so that the contact person really exists before inserting the seller
            //and also get the has ordered so that you prevent the seller from being added if the order is already sent
            $idTestQuery = "SELECT `contact_id`, `hasOrdered` FROM `contact` WHERE `contact_id` = '".$contact_id."' AND `isActive` = 1 LIMIT 1";
            //echo($idTestQuery);
            $result = mysqli_query($db, $idTestQuery);
            //print_r($result);
            $rowHasOrdered = mysqli_fetch_array($result);
            //echo($rowHasOrdered[0]." ".$rowHasOrdered[1]);
            //echo(sizeof($rowHasOrdered));
            
            //if seller exists and seller hasn't ordered yet
            if(sizeof($rowHasOrdered) > 0 && $rowHasOrdered[1] == 0) {
                //insert seller into db
                
                if(mysqli_query($db, $query)) {
                    echo("<div class='alert alert-info container'>Säljare<strong> ".$first_name." </strong>är nu registrerad!<div id='seller_pw' class='ten-down' style='color:blue;'>Ditt lösenord är: <h2 class='password'>".$pw."</h2></div><br /><a href='index.php'><button type='button' class='btn btn-primary ten-down'>Till log-in sidan</button></a></div>");
                
                } else {
                    echo("<div class='alert alert-danger container'>Något blev fel vid registreringen.<br />Försök igen om en liten stund.");
                }
            } else {
                echo("<div class='alert alert-danger container'>Din kontaktperson existerar inte eller så har försäljningen redan avslutats!<br />Kontakta mixboxen.se för hjälp.</div>");
                //here is where you end up if the contact against all ods shouldn't be in the db
            }
        }   //end of seller registration 
    } else { 
        echo("<div class='alert alert-danger container'>Du har skrivit fel på någon av uppgifterna!<br />Kom ihåg att alla fälten måste vara ifyllda.<br />Gå tillbaka och försök igen!");
    }               
} else {
    echo("<div class='container'>Since you ended up here something went terribly wrong!<br />Try to go back to the previous page.<br />Best of luck!/Kermit<br /><a href='index.php'><button type='button' class='btn btn-danger ten-down'>Tillbaka</button></a></div>");
}

mysqli_close($db);

function getTimeAndDate() {
    $timeAndDate = date("Y-m-d H:i:sa");
    return $timeAndDate;
}

function isValidatedContact($first_name, $last_name, $street_address, $zip_code, $city, $email, $phone_number, $association) {
    
    $valid = false;
    
    if($first_name != "" && $last_name != "" && $street_address != "" && $zip_code != "" && $city != "" && $email != "" && $phone_number != "" && $association) {
        $valid = true;
    }
    
    if (!preg_match("/^[a-zA-Z åäöÅÄÖ]*$/",$first_name)) {
        $valid = false; 
    }
    
    if (!preg_match("/^[a-zA-Z åäöÅÄÖ]*$/",$last_name)) {
        $valid = false; 
    }
    
    if (!preg_match("/^[a-zA-Z åäöÅÄÖ 0-9]*$/",$street_address)) {
        $valid = false; 
    }
    
    if (!preg_match("/^[ 0-9]*$/", $zip_code)) {
        $valid = false;
    }
    
    if (!preg_match("/^[a-zA-Z åäöÅÄÖ 0-9]*$/",$city)) {
        $valid = false; 
    }
    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $valid = false;
    }
    
    if (!preg_match("/^[0-9 + -]*$/", $phone_number)) {
        $valid = false;
    }
    
    return $valid;
    
}

function isValidated($first_name, $last_name, $phone_nr, $email) {
    
    $valid = false;
    
    if($first_name != "" && $last_name != "" && $phone_nr != "" && $email != "") {
        $valid = true;
    }
    
    if (!preg_match("/^[a-zA-Z åäöÅÄÖ]*$/",$first_name)) {
        $valid = false; 
    }
    
    if (!preg_match("/^[a-zA-Z åäöÅÄÖ]*$/",$last_name)) {
        $valid = false; 
    }
    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $valid = false;
    }
    
    if (!preg_match("/^[-0-9]*$/", $phone_nr)) {
        $valid = false;
    }
    
    return $valid;
}

?>

<html>
    <head>
        <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
        <style type="text/css">
            
            .gradient {
              background: linear-gradient(white, gray);
            }
        
            #code {
                width: 100%;
                font-size: 1.2em;
            }
            
            .container {
                text-align: center;
                margin-top: 3%;
            }
            
            #password {
                margin-bottom: 15%;
            }
            
            .password {
                font-family: monospace;
            }
            
            .ten-down {
                margin-top: 10%;
            }
            
        
        </style>
    </head>
    <body class="gradient">
        
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    </body>
</html>