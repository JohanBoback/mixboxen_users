<?php

include("db_login.php");

$selectString = "";

if(array_key_exists("content", $_POST)) {

    $sortVariable = $_POST["content"];
    
    if($sortVariable <= 0 || $sortVariable >= 6) {
        $sortVariable = 6;
    }
    
    switch($sortVariable) {
        case "1":
            $selectString = "first_name";
            break;
        case "2":
            $selectString = "last_name";
            break;
        case "3":
            $selectString = "email";
            break;
        case "4":
            $selectString = "phone_number";
            break;
        case "5":
            $selectString = "association";
            break;
        case "6":
            $selectString = "contact_id";
            break;    
    }
    
//build the table of contact persons
$query = "SELECT `first_name`, `last_name`, `email`, `phone_number`, `association`, `contact_id`, `hasOrdered` FROM `contact` WHERE `isActive` = 1 ORDER BY `".$selectString."`";

$rowArray = mysqli_fetch_all(mysqli_query($db, $query));

$contactTable = "";
//loop through the array[][] of arrays each one containing one row from the db 
for($i = 0; $i < sizeof($rowArray); $i++) {
    if($rowArray[$i][6] == 1) {
        $contactTable.= "<tr style='font-style:italic; color:firebrick' name=".$rowArray[$i][5]."><td>".$rowArray[$i][0]."</td><td>".$rowArray[$i][1]."</td><td>".$rowArray[$i][2]."</td><td>".$rowArray[$i][3]."</td><td id='association'>".$rowArray[$i][4]."</td><td id='id'>".$rowArray[$i][5]."</td></tr>";
    } else {
        $contactTable.= "<tr name=".$rowArray[$i][5]."><td>".$rowArray[$i][0]."</td><td>".$rowArray[$i][1]."</td><td>".$rowArray[$i][2]."</td><td>".$rowArray[$i][3]."</td><td id='association'>".$rowArray[$i][4]."</td><td id='id'>".$rowArray[$i][5]."</td></tr>";
    }
}
    
mysqli_close($db);    

echo($contactTable);    

    
    
}

?>