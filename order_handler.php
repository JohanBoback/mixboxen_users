<?php

session_start();

include("db_login.php");



if(isset($_POST["tableArray"])) {

    $query = "SELECT `first_name`, `last_name`, `street_address`, `zip_code`, `city`, `email`, `phone_number`, `association`, `hasOrdered` FROM `contact` WHERE `contact_id` = '".mysqli_real_escape_string($db, $_SESSION["contact_id"])."' LIMIT 1";


    $result = mysqli_query($db, $query);
    $row = mysqli_fetch_array($result);
    
    if($row[8] == 0) {

        $outString = "";

        $infoArray = array("Firstname:\t", "Lastname:\t", "Street:\t", "ZIP:\t", "City:\t", "E-mail:\t", "Phone:\t", "Association:\t");

        for($i = 0; $i < 8; $i++) {
            $outString.=$infoArray[$i].$row[$i]."<br />";
        }

        $outString.= "<br />";
        $outString.= "-------------------------------------<br />NR OF BOXES/SELLER<br />-------------------------------------<br />";

        //create a temporary table to use only for sorting data
        $tempTableQuery = "CREATE TABLE `".$row["first_name"].$_SESSION["contact_id"]."` (
        `id` INT AUTO_INCREMENT PRIMARY KEY,
        `nr_of_boxes` INT,
        `mix_box` VARCHAR(50),
        `box_art_nr` INT,
        `seller_id` INT
        );";
        mysqli_query($db, $tempTableQuery);

        //skriv ut den första tabellen samt sätt in den i en ny table för att kunna summera beställningarna
        for($i = 2; $i < sizeof($_POST["tableArray"]); $i++) {
//            for($j = 0; $j < 4; $j++) {
//                $outString.= ($_POST["tableArray"][$i][$j])." -- "; 
//            }
//            $outString.="<br />";
            //create the query and insert the rows into the temp table

            $insertQuery = "INSERT INTO `".$row["first_name"].$_SESSION["contact_id"]."` (`id`, `nr_of_boxes`, `mix_box`, `box_art_nr`, `seller_id`) VALUES (NULL, '".$_POST["tableArray"][$i][0]."', '".$_POST["tableArray"][$i][1]."', '".$_POST["tableArray"][$i][2]."', '".$_POST["tableArray"][$i][3]."');";
            mysqli_query($db, $insertQuery);
        }

        //echo($outString);
        //create the first table with boxes summed/seller
        $tableOneQuery = "SELECT SUM( `nr_of_boxes` ) , `seller_id` FROM `".$row["first_name"].$_SESSION["contact_id"]."` GROUP BY `seller_id` ORDER BY SUM( `nr_of_boxes` ) DESC; ";
        $tableOneRes = mysqli_query($db, $tableOneQuery);
        $tableOneRowArray = mysqli_fetch_all($tableOneRes);
        
        $tableOneString = "";

        for($i = 0; $i < sizeof($tableOneRowArray); $i++) {
                $tableOneString.="Box summary: ".$tableOneRowArray[$i][0]." -- sellerID --> ".$tableOneRowArray[$i][1]; 
            $tableOneString.="<br />";
        }
        //get the orderlist from the new table and make an "e-mailable" table

        $tableTwoQuery = "SELECT SUM( `nr_of_boxes` ) , `mix_box`, `box_art_nr` FROM `".$row["first_name"].$_SESSION["contact_id"]."` GROUP BY `mix_box` ORDER BY SUM( `nr_of_boxes` ) DESC; ";
        $tableTwoRes = mysqli_query($db, $tableTwoQuery);
        $tableTwoRowArray = mysqli_fetch_all($tableTwoRes);


        //iterate through the table array and make a readable mailable string
        $tableTwoString = "";

        for($i = 0; $i < sizeof($tableTwoRowArray); $i++) {
                $tableTwoString.= $tableTwoRowArray[$i][0]." -- ".$tableTwoRowArray[$i][1]." -- ".$tableTwoRowArray[$i][2]."  ".$tableTwoRowArray[$i][3]; 
            $tableTwoString.="<br />";
        }

        //drop the temp table since we don't need it anymore

        mysqli_query($db, "DROP TABLE `".$row["first_name"].$_SESSION["contact_id"]."`");
            
            //$association = utf8_decode($row[7]);

            $emailTo = "orders@mixboxen.se";              //orders@mixboxen.se
            $subject = "Order from: ".utf8_decode($row[7]);
            $body = "<html><body>\r\n";
            $body .= "<div style='font-size: 16px'>";
            $body .= utf8_decode($outString.$tableOneString."-------------------------------------<br />TOTAL SUMMARY:<br />-------------------------------------<br />".$tableTwoString);
            $body .= "</div></body></html>";
            $headers = "from: info@mixboxen.se\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if(isContactActive($db)) {
        //test
            if(mail($emailTo, $subject, $body, $headers)) {

                echo("<div class='alert alert-success'><h1>Your order was placed successfully!</h1></div>");
                saleEnded($db);

            } else {

                echo("<h1 style='color: red'>An error occurred while placing your order!<br />Please try again.</h1>");

            }
        } else {
            echo("<h1 style='color: red'>Your account are not active anymore!<br />Please contact mixboxen.se</h1>");
        }
    } else {
        echo("<div class='alert alert-warning'><h1>Ordern är redan beställd!<br />Kontakta mixboxen.se för hjälp.</h1></div>");
    }

} else {        //end of isset()
    echo("<h1 style='color: red'>Something went wrong!<br />Please contact mixboxen.se</h1>");
}

mysqli_close($db);

function saleEnded($db) {
    $successQuery = "UPDATE `contact` SET `hasOrdered` = '1' WHERE `contact_id` = '".$_SESSION["contact_id"]."' LIMIT 1";
    mysqli_query($db, $successQuery);
    setSellerCanSell($db);
}

function setSellerCanSell($db) {
    $sellQuery = "UPDATE `seller` SET `canSell` = '0' WHERE `contact_id` = '".$_SESSION["contact_id"]."';";
    $sellRes = mysqli_query($db, $sellQuery);
}

function isContactActive($db) {
    //there is a session ID, let's check so that the user hasn't been deleted while still logged in
    $isActiveQuery = "SELECT * FROM `contact` WHERE `contact_id` = '".mysqli_real_escape_string($db, $_SESSION["contact_id"])."' AND `isActive` = '1' LIMIT 1";
    
    $isActiveResult = mysqli_query($db, $isActiveQuery);
    $isActiveRow = mysqli_fetch_array($isActiveResult);
    
   //echo(sizeof($isActiveRow));
    
    if(sizeof($isActiveRow) > 0) {
        return true;
        //echo("true");
    } else {
        //echo("false");
        return false;
        
    }
}

?>