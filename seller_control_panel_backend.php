<?php

include("db_login.php");

session_start();

if(array_key_exists("content", $_POST)) {
    
    $boxesTable = "";
    $selectString = "";
    $sumBoxes = 0;
    
    $sortVariable = $_POST["content"];
    
//    if($sortVariable <= 0 || $sortVariable >= 6) {
//        $sortVariable = "5";
//    }
    
    switch($sortVariable) {
        case "1":
            $selectString = "nr_of_boxes";
            break;
        case "2":
            $selectString = "mix_box";
            break;
        case "3":
            $selectString = "box_art_nr";
            break;
//        case "4":
//            $selectString = "buyer";
//            break;
        case "5":
            $selectString = "date";
            break; 
        case "6":
            $selectString = "id";
            break;
        default: 
            $selectString = "date";
            break;
    }

    //lets get some stuff from the database regarding sold objects
    $soldQuery = "SELECT `nr_of_boxes`, `mix_box`, `box_art_nr`, `date`, `id` FROM `sellers_list` WHERE `seller_id` = '".mysqli_real_escape_string($db, $_SESSION["seller_id"])."' AND `isActive` = 1 ORDER BY `".$selectString."`";

    $result = mysqli_query($db, $soldQuery);
    //convert the result to a php multidimensional array
    $rowsArray = mysqli_fetch_all($result);

    for($i = 0; $i < sizeof($rowsArray); $i++) {
        $sumBoxes += $rowsArray[$i][0];
        $boxesTable.="<tr><td>".$rowsArray[$i][0]."</td><td>".$rowsArray[$i][1]."</td><td>".$rowsArray[$i][2]."</td><td>".$rowsArray[$i][3]."</td><td>".$rowsArray[$i][4]."</td></tr>";
    }

    //mysqli_close($db);
    //end of rendering the box table
echo(json_encode(array($boxesTable, $sumBoxes)));
    
}






?>