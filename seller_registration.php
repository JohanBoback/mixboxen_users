<?php



include("db_login.php");


//check so that the link has a key named contact_id
if(isset($_GET["checksum"])) {
    
    //get the contact_id out of the GET variable
    $tempString = $_GET["checksum"];
    //substr from char nr 3 to char strlen - 6 starting from 0
    $contact_id = substr($tempString, 4, (strlen($tempString)-8));
    //end of contact_id cut
    //echo($contact_id);
    //eliminate the risk of getting an old link by checking if the contact_id is in the db
    $query = "SELECT `contact_id` FROM `contact` WHERE `contact_id` = '".mysqli_real_escape_string($db, $contact_id)."' AND `isActive` = 1 LIMIT 1";
    $result = mysqli_query($db, $query);
    
    if(mysqli_num_rows($result) > 0) {
        
        $query = "SELECT `first_name`, `last_name`, `phone_number` FROM `contact` WHERE '".mysqli_real_escape_string($db, $contact_id)."' = `contact_id` AND `isActive` = 1 LIMIT 1";

        $result = mysqli_query($db, $query);

        $row = mysqli_fetch_array($result);
        
    } else {
        mysqli_close($db);
        header("Location: old_id_error.php");
    }
    //end of old link check
} else {
    mysqli_close($db);
    header("Location: get_variable_error.php");
}

mysqli_close($db);
?>

<html>

    <head>
    
        <title>Seller Registration</title>
    
        <meta http-equiv="content-type" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <script src="https://use.fontawesome.com/34eb041fe3.js"></script>
        <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">

        
        <style type="text/css">
            
            .gradient {
              background: linear-gradient(white, #e0e0e0);
            } 
        
            #header-registration {
                margin-bottom: 4%;
                margin-top: 5%;
            }
            
            #register-btn {
                margin-top: 20px;
            }
            
            label {
                margin-top: 8px;
            }
            
            button {
                background-color: #cc733d !important;
                border-color: #cc733d !important;
            }
            
            button:hover {
                background-color: #BB5E31 !important;
            }
        
        </style>
        
    </head>
    <body class="gradient">
    
        <div class="container">
            
            <h2 id="header-registration">MIXBOXEN --> registrering av säljare</h2>
        
            <form id="seller-form" method="POST" action="db_communication.php" autocomplete="off">    
        
            <div class="form-group row">
              <label for="name-input" class="col-md-2 col-form-label">Förnamn</label>
              <div id="first_name" class="col-md-6">
                <input name="first_name" class="form-control" type="text" placeholder="Mix" id="name-input" autofocus>
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="text-input" class="col-md-2 col-form-label">Efternamn</label>
              <div id="last_name" class="col-md-6">
                <input name="last_name" class="form-control" type="text" placeholder="Box" id="text-input">
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="tel-input" class="col-md-2 col-form-label">Telefonnummer</label>
              <div id="phone_number" class="col-md-6">
                <input name="phone_number" class="form-control" type="text" placeholder="012-3456789" id="tel-input">
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="email-input" class="col-md-2 col-form-label">E-mail</label>
              <div id="email" class="col-md-6">
                <input name="email" class="form-control" type="email" placeholder="info@mixboxen.se" id="email-input">
                <div class='form-control-feedback'></div>  
              </div>
            </div>
              
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kontaktperson</label>
              <div class="col-md-6">
                <input name="contact_person" class="form-control" type="text" value="<?php echo($row["first_name"]." ".$row["last_name"]." : Tel. ".$row["phone_number"]) ?>" id="contact-person" readonly>
              </div>
            </div>
            <input name="contact_id" class="form-control" type="text" value="<?php echo($contact_id) ?>" readonly hidden="true">
            <div class="form-group row">
                <div class="offset-md-2 col-md-10">
                    <button name="submit_seller" id="register-btn" type="submit" class="btn btn-success" >Registrera</button>
                </div>
            </div>
            </form>
        </div>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="jquery.min.js"></script>
        <script src="tether.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <script type="text/javascript">
        
            $("#seller-form").submit(function(event){
                //console.log(validateForm());
                return validateForm(); //if false the form won't submit    
            });
            
            
            $('input').on('input', function(event){
                    liveValidation(event.target.name);
            });
            
            function liveValidation(name) {
                
                switch(name) {
                    case "first_name":
                        if(!isLetter($('#first_name input').val())) {
                            isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Inga siffror tillåtna i förnamnet');
                            if($('#first_name input').val() == "") {
                                isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Du måste skriva ett förnamn');
                            }
                        } else {
                            isValid = displaySuccess('#first_name', '#first_name input', '#first_name div');
                        }
                        break;
                    case "last_name":
                        if(!isLetter($('#last_name input').val())) {
                            isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Siffror är inte tillåtet i efternamnet');
                            if($('#last_name input').val() == "") {
                                isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Du måste skriva ett efternamn');  
                        }
                        } else {
                            isValid = displaySuccess('#last_name', '#last_name input', '#last_name div');
                        }
                        break;
                    case "phone_number":
                        if(!isPhoneNumber($('#phone_number input').val())) {
                            isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Otillåtet telefonnummer');
                            if($('#phone_number input').val() == "") {
                                isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Det krävs ett telefonnummer');
                            }
                        } else {
                            isValid = displaySuccess('#phone_number', '#phone_number input', '#phone_number div');
                        }
                        break;
                    case "email":
                        if(!isEmail($('#email input').val())) {
                            isValid = displayError('#email', '#email input', '#email div', 'Felaktig e-post adress');
                            if($('#email input').val() == "") {
                                isValid = displayError('#email', '#email input', '#email div', 'Du måste fylla i en e-postadress');
                            }
                        } else {
                            isValid = displaySuccess('#email', '#email input', '#email div');
                        }
                        break;    
                } //end of switch
            }//end of live validation
            
            function validateForm() {
                
                var isValid = true;
                
                if(!isLetter($('#first_name input').val())) {
                    isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Inga siffror tillåtna i förnamnet');
                    if($('#first_name input').val() == "") {
                        isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Du måste skriva ett förnamn');
                    }
                } else {
                    isValid = displaySuccess('#first_name', '#first_name input', '#first_name div');
                }
                
                if(!isLetter($('#last_name input').val())) {
                    isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Siffror är inte tillåtet i efternamnet');
                    if($('#last_name input').val() == "") {
                        isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Du måste skriva ett efternamn');  
                }
                } else {
                    isValid = displaySuccess('#last_name', '#last_name input', '#last_name div');
                }
                
                if(!isPhoneNumber($('#phone_number input').val())) {
                    isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Otillåtet telefonnummer');
                    if($('#phone_number input').val() == "") {
                        isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Det krävs ett telefonnummer');
                    }
                } else {
                    isValid = displaySuccess('#phone_number', '#phone_number input', '#phone_number div');
                }
                
                if(!isEmail($('#email input').val())) {
                    isValid = displayError('#email', '#email input', '#email div', 'Felaktig e-post adress');
                    if($('#email input').val() == "") {
                        isValid = displayError('#email', '#email input', '#email div', 'Du måste fylla i en e-postadress');
                    }
                } else {
                    isValid = displaySuccess('#email', '#email input', '#email div');
                }
                return isValid;
                
            }// end of validate form
            
            function displayError(parent, child, messageDiv, message) {
                if($(parent).hasClass('has-success')) {
                   $(parent).removeClass('has-success'); 
                }
                if($(child).hasClass('form-control-success')) {
                   $(child).removeClass('form-control-success'); 
                }
                $(parent).addClass('has-danger');    
                $(child).addClass('form-control-danger');    
                $(messageDiv).show();
                $(messageDiv).text(message);
                return false;
            }
            
            function displaySuccess(parent, child, messageDiv) {
                if($(parent).hasClass('has-danger')) {
                   $(parent).removeClass('has-danger'); 
                }
                if($(child).hasClass('form-control-danger')) {
                   $(child).removeClass('form-control-danger'); 
                }
                $(parent).addClass('has-success');    
                $(child).addClass('form-control-success');
                $(messageDiv).hide();
                return true;
            }
            
            function isEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }
            
            function isPhoneNumber(number) {
                var regex = /^[0-9 + -]+$/;
                return regex.test(number);
            }
            
            function isZip(number) {
                var regex = /^[0-9]+$/;
                return regex.test(number);
            }
            
            function isLetter(number) {
                var regex = /^[a-zA-Z åäö ÅÄÖ]+$/;
                return regex.test(number);
            }
            
            function isLettersAndNumbers(number) {
                var regex = /^[a-zA-Z åäö ÅÄÖ 0-9]+$/;
                return regex.test(number);
            }
        
            
        </script>
    </body>

</html>