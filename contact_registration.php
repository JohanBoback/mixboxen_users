<?php


include("db_login.php");

if(isset($_GET["login"])) {

    $query = "SELECT `id` FROM `contact_signup_passwords` WHERE `temp_passwords` = '".mysqli_real_escape_string($db, $_GET["login"])."' LIMIT 1";

    $result = mysqli_query($db, $query);

    //print_r($result);

    if(mysqli_num_rows($result) <= 0) {
        mysqli_close($db);
        header("Location: contact_signup_error.php");
    }

    $temp_passwords = $_GET["login"];

} else {
    header("Location: contact_signup_error.php");
}
 
mysqli_close($db);
?>

<html>

    <head>
    
        <title>Contact Registration</title>
    
        <meta http-equiv="content-type" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <script src="https://use.fontawesome.com/34eb041fe3.js"></script>
        <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">

       
        
        <style type="text/css">
            
            .gradient {
              background: linear-gradient(white, #e0e0e0);
            }
        
        
            #header-registration {
                margin-bottom: 4%;
                margin-top: 3%;
            }
            
            #register-btn {
                margin-top: 2%;
            }
            
            #register-btn {
                background-color: #cc733d !important;
                border-color: #cc733d !important;
            }
            
            #register-btn:hover {
                background-color: #BB5E31 !important;
            }
            
            #clear-form-btn {
                float: right;
                margin-right: 41%;
                margin-top: 2%;
            }
            
        
        </style>
        
    </head>
    <body class="gradient">
    
        <div class="container">
            
            <h2 id="header-registration">MIXBOXEN --> registrering av kontaktperson</h2>
            
            <form id="contact-form" method="POST" action="db_communication.php" autocomplete="off">
            <div class="form-group row">
              <label for="example-text-input" class="col-md-2 col-form-label">Förnamn</label>
              <div id="first_name" class="col-md-6">
                <input name="first_name" class="form-control" type="text" placeholder="Mix">
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="example-text-input" class="col-md-2 col-form-label">Efternamn</label>
              <div id="last_name" class="col-md-6">
                <input name="last_name" class="form-control" type="text" placeholder="Box"  >
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="example-text-input" class="col-md-2 col-form-label">Gatuadress</label>
              <div id="street_address" class="col-md-6">
                <input name="street_address" class="form-control" type="text" placeholder="Boxgatan 1"  >
                <div class='form-control-feedback'></div>  
              </div>
            </div>

            <div class="form-group row">
              <label for="example-email-input" class="col-md-2 col-form-label">Postnummer</label>
              <div id="zip_code" class="col-md-6">
                <input name="zip_code" class="form-control" type="text" placeholder="654321"  >
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="example-email-input" class="col-md-2 col-form-label">Postadress</label>
              <div id="city" class="col-md-6">
                <input name="city" class="form-control" type="text" placeholder="Boxholm" >
                <div class='form-control-feedback'></div>  
              </div>
            </div>

            <div class="form-group row">
              <label for="example-tel-input" class="col-md-2 col-form-label">Telefonnummer</label>
              <div id="phone_number" class="col-md-6">
                <input name="phone_number" class="form-control" type="text" placeholder="012-3456789" id="tel-input" >
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="example-email-input" class="col-md-2 col-form-label">E-mail</label>
              <div id="email" class="col-md-6">
                <input name="email" class="form-control" type="text" placeholder="info@mixboxen.se" id="email-input" >
                <div class='form-control-feedback'></div>  
              </div>
            </div>
            
            <div class="form-group row">
              <label for="example-email-input" class="col-md-2 col-form-label">Förening/Lag/Klass</label>
              <div id="association" class="col-md-6">
                <input name="association" class="form-control" type="text" placeholder="IFK Boxen"  >
                <div class='form-control-feedback'></div>  
              </div>
            </div>

            <div class="form-group row">
                <div class="offset-md-2 col-md-10">
                    <button id="register-btn" type="submit" name="submit_contact" class="btn btn-success">Registrera</button>
                    <button id="clear-form-btn" class="btn btn-danger" type="button">Rensa fält</button>
                </div>
            </div>
            <input id="signup_password" name="signup_password" class="form-control" type="text" value="<?php echo($temp_passwords) ?>" readonly hidden>
            </form>
        </div>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    
        <script type="text/javascript">
            
            $("#contact-form").submit(function(event){
                //console.log(validateForm());
                return validateForm(); //if false the form won't submit    
            });
        
            $('#clear-form-btn').click(function(){
                $('input').val('');
                $('#signup_password').val('<?php echo($temp_passwords) ?>');
            });
            
            $('input').on('input', function(event){
                    liveValidation(event.target.name);
            });
            
            function liveValidation(name) {
                
                switch(name) {
                    case "first_name":
                        if(!isLetter($('#first_name input').val())) {
                            isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Inga siffror tillåtna i förnamnet');
                            if($('#first_name input').val() == "") {
                                isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Du måste skriva ett förnamn');
                            }
                        } else {
                            isValid = displaySuccess('#first_name', '#first_name input', '#first_name div');
                        }
                        break;
                    case "last_name":
                        if(!isLetter($('#last_name input').val())) {
                            isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Siffror är inte tillåtet i efternamnet');
                            if($('#last_name input').val() == "") {
                                isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Du måste skriva ett efternamn');  
                        }
                        } else {
                            isValid = displaySuccess('#last_name', '#last_name input', '#last_name div');
                        }
                        break;
                    case "street_address":
                        if($('#street_address input').val() == "") {
                            isValid = displayError('#street_address', '#street_address input', '#street_address div', 'Gatuadress krävs');  
                        } else {
                            isValid = displaySuccess('#street_address', '#street_address input', '#street_address div');
                        }
                        break;
                    case "zip_code":    
                        if(!isZip($('#zip_code input').val())) {
                            isValid = displayError('#zip_code', '#zip_code input', '#zip_code div', 'Endast siffror i postnumret');
                            if($('#zip_code input').val() == "") {
                                isValid = displayError('#zip_code', '#zip_code input', '#zip_code div', 'Postnummer krävs');
                            }
                        } else {
                            isValid = displaySuccess('#zip_code', '#zip_code input', '#zip_code div');
                        }
                        break;
                    case "city":
                        if(!isLetter($('#city input').val())) {
                            isValid = displayError('#city', '#city input', '#city div', 'Inga siffror i postadressen');
                            if($('#city input').val() == "") {
                                isValid = displayError('#city', '#city input', '#city div', 'Du måste skriva en postadress');
                            }
                        } else {
                            isValid = displaySuccess('#city', '#city input', '#city div');
                        }
                        break;
                    case "phone_number":
                        if(!isPhoneNumber($('#phone_number input').val())) {
                            isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Otillåtet telefonnummer');
                            if($('#phone_number input').val() == "") {
                                isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Det krävs ett telefonnummer');
                            }
                        } else {
                            isValid = displaySuccess('#phone_number', '#phone_number input', '#phone_number div');
                        }
                        break;
                    case "email":
                        if(!isEmail($('#email input').val())) {
                            isValid = displayError('#email', '#email input', '#email div', 'Felaktig e-post adress');
                            if($('#email input').val() == "") {
                                isValid = displayError('#email', '#email input', '#email div', 'Du måste fylla i en e-postadress');
                            }
                        } else {
                            isValid = displaySuccess('#email', '#email input', '#email div');
                        }
                        break;
                    case "association":
                        if($('#association input').val() == "") {
                            isValid = displayError('#association', '#association input', '#association div', 'Du måste fylla i ett lag/klass/förening');  
                        } else {
                            isValid = displaySuccess('#association', '#association input', '#association div');
                        }
                        break;
                        
                    }//end of switch
            }
            
            function validateForm() {
                
                var isValid = true;
                
                if(!isLetter($('#first_name input').val())) {
                    isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Inga siffror tillåtna i förnamnet');
                    if($('#first_name input').val() == "") {
                        isValid = displayError('#first_name', '#first_name input', '#first_name div', 'Du måste skriva ett förnamn');
                    }
                } else {
                    isValid = displaySuccess('#first_name', '#first_name input', '#first_name div');
                }
                
                if(!isLetter($('#last_name input').val())) {
                    isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Siffror är inte tillåtet i efternamnet');
                    if($('#last_name input').val() == "") {
                        isValid = displayError('#last_name', '#last_name input', '#last_name div', 'Du måste skriva ett efternamn');  
                }
                } else {
                    isValid = displaySuccess('#last_name', '#last_name input', '#last_name div');
                }
                
                if($('#street_address input').val() == "") {
                    isValid = displayError('#street_address', '#street_address input', '#street_address div', 'Gatuadress krävs');  
                } else {
                    isValid = displaySuccess('#street_address', '#street_address input', '#street_address div');
                }
                
                if(!isZip($('#zip_code input').val())) {
                    isValid = displayError('#zip_code', '#zip_code input', '#zip_code div', 'Endast siffror i postnumret');
                    if($('#zip_code input').val() == "") {
                        isValid = displayError('#zip_code', '#zip_code input', '#zip_code div', 'Postnummer krävs');
                    }
                } else {
                    isValid = displaySuccess('#zip_code', '#zip_code input', '#zip_code div');
                }
                
                if(!isLetter($('#city input').val())) {
                    isValid = displayError('#city', '#city input', '#city div', 'Inga siffror i postadressen');
                    if($('#city input').val() == "") {
                        isValid = displayError('#city', '#city input', '#city div', 'Du måste skriva en postadress');
                    }
                } else {
                    isValid = displaySuccess('#city', '#city input', '#city div');
                }
                
                if(!isPhoneNumber($('#phone_number input').val())) {
                    isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Otillåtet telefonnummer');
                    if($('#phone_number input').val() == "") {
                        isValid = displayError('#phone_number', '#phone_number input', '#phone_number div', 'Det krävs ett telefonnummer');
                    }
                } else {
                    isValid = displaySuccess('#phone_number', '#phone_number input', '#phone_number div');
                }
                
                if(!isEmail($('#email input').val())) {
                    isValid = displayError('#email', '#email input', '#email div', 'Felaktig e-post adress');
                    if($('#email input').val() == "") {
                        isValid = displayError('#email', '#email input', '#email div', 'Du måste fylla i en e-postadress');
                    }
                } else {
                    isValid = displaySuccess('#email', '#email input', '#email div');
                }
           
                if($('#association input').val() == "") {
                    isValid = displayError('#association', '#association input', '#association div', 'Du måste fylla i ett lag/klass/förening');  
                } else {
                    isValid = displaySuccess('#association', '#association input', '#association div');
                }
                
                return isValid;
            }
            
            function displayError(parent, child, messageDiv, message) {
                if($(parent).hasClass('has-success')) {
                   $(parent).removeClass('has-success'); 
                }
                if($(child).hasClass('form-control-success')) {
                   $(child).removeClass('form-control-success'); 
                }
                $(parent).addClass('has-danger');    
                $(child).addClass('form-control-danger');    
                $(messageDiv).show();
                $(messageDiv).text(message);
                return false;
            }
            
            function displaySuccess(parent, child, messageDiv) {
                if($(parent).hasClass('has-danger')) {
                   $(parent).removeClass('has-danger'); 
                }
                if($(child).hasClass('form-control-danger')) {
                   $(child).removeClass('form-control-danger'); 
                }
                $(parent).addClass('has-success');    
                $(child).addClass('form-control-success');
                $(messageDiv).hide();
                return true;
            }
            
            function isEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }
            
            function isPhoneNumber(number) {
                var regex = /^[0-9 + -]+$/;
                return regex.test(number);
            }
            
            function isZip(number) {
                var regex = /^[0-9]+$/;
                return regex.test(number);
            }
            
            function isLetter(number) {
                var regex = /^[a-zA-Z åäö ÅÄÖ]+$/;
                return regex.test(number);
            }
            
            function isLettersAndNumbers(number) {
                var regex = /^[a-zA-Z åäö ÅÄÖ 0-9]+$/;
                return regex.test(number);
            }
        
        </script>
        
    </body>

</html>