<?php

include("db_login.php");

//echo($_POST["email"]." ".$_POST["typeOfPerson"]." ".$_POST["firstName"]);


if(isset($_POST["email"])) {
    
    //echo($_POST["email"]." ".$_POST["typeOfPerson"]." ".$_POST["firstName"]);
    
    $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $newPassword = getNewPassword();
    $user = ($_POST["typeOfPerson"] == "true" ? "seller" : "contact");
    
    //echo($user);
    
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo("wrong email format");
    }
    
    
    
    //if typeOfPerson == true it's a seller. Insert the new seller-password and also mail it to the user
    if($_POST["typeOfPerson"] == "true") {
        
        $query = "UPDATE `seller` SET `password` = '".$newPassword."' WHERE `email` = '".mysqli_real_escape_string($db, $email)."' AND `first_name` = '".mysqli_real_escape_string($db, $_POST["firstName"])."' AND `isActive` = 1 LIMIT 1;";
        
        if(mysqli_query($db, $query)) {
            
            if(sendEmail($email, $newPassword, $user)) {
                echo("<div class='alert alert-success'>"."Nytt lösenord skickat!"."</div>");
            } else {
                echo("<div class='alert alert-danger'>"."Oops! Något blev fel<br />Försök igen!"."</div>");
            }
        }
        
    } else {  //same thing as above but for the contact alternative
        
        $query = "UPDATE `contact` SET `password` = '".$newPassword."' WHERE `email` = '".mysqli_real_escape_string($db, $email)."' AND `isActive` = 1 LIMIT 1;";
        
        //echo($query);
        
        if(mysqli_query($db, $query)) {
            
            if(sendEmail($email, $newPassword, $user)) {
                echo("<div class='alert alert-success'>"."Nytt lösenord skickat!"."</div>");
            } else {
                echo("<div class='alert alert-danger'>"."Oops! Något blev fel<br />Försök igen!"."</div>");
            }
        }
    }
    

    
mysqli_close($db);    

}

function sendEmail($email, $password, $user) {
    
    $emailTo = $_POST['email'];
    $subject = "New password from mixboxen.se";
    $body = "New password for ".$user." with email: ".$email."  --->  ".$password;
    $headers = "from: info@mixboxen.se";

    if(mail($emailTo, $subject, $body, $headers)) {

        return true;

    } else {

        return false;

    }
}

function getNewPassword() {
    
    $alpha = "abcdefghijklmnopqrstuvwxyz";
    $alpha_upper = strtoupper($alpha);
    $numeric = "0123456789";
    $special = ".-+=_,!@$#*%<>[]{}";
    $chars = "";


    $chars = $alpha.$alpha_upper.$numeric;
    $length = 7;


    $len = strlen($chars);
    $pw = '';

    for ($i=0;$i<$length;$i++)
            $pw .= substr($chars, rand(0, $len-1), 1);

    // the finished password
    $pw = str_shuffle($pw);

    return $pw;
    
}
?>