
<html>
    <head>
        <title>Frontpage Mixboxen</title>
        
        <meta http-equiv="content-type" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <script src="https://use.fontawesome.com/34eb041fe3.js"></script>
        
        <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">

        
        
<!--        <link rel="stylesheet" href="test_box.css">-->
        
        <style type="text/css">
            
             html { 
                background: url(flower_one_small.jpg) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                overflow-x: hidden;
        
            }
            
            body {
                background: none !important;
            }
            
            #logo-div {
                
                float:left;
/*                text-align: center;*/
            }
            
            #menu {
                font-size: 3em;
                float: right;
                margin-right: 8%;
                margin-top: 20%;
            }
            
            img {
                width: 80%;
                opacity: .6;
            }
            
            ul {
                background: none !important;
                background-color: none;
            }
            
            li {
                opacity: .4;
                background-color:darkgoldenrod !important;
                color: palegoldenrod !important;
                border-color: goldenrod !important;
            }
            
            a:visited {
                color: darkgoldenrod;
            }
            
            a {    
                color: darkgoldenrod;
            }
            
            a:hover {
                color: goldenrod;
            }
            
            a:active {
                color: darkgoldenrod;
            }
            
            h3 {
                
                margin-top: 0%;
            }
            
            .center {
                float: right;
                color: #BF6635;
            }
            
        </style>
    </head>
    <body>
        <div class="row">
            <div class="col-md-6" id="logo-div">
                <img src="mix_transparent_no_border.png">    
            </div>
            
            <div class="col-md-6" id="rigth-menu-div">
                
                <a href="#"><i id="menu" class="fa fa-bars" aria-hidden="true" data-toggle="modal" data-target=".bd-example-modal-md"></i></a>
            </div>
        </div>
        

        <div class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
        <div class="modal-content">
            <ul class="list-group">
              <a href=""><li class="list-group-item">Cras justo odio</li></a>
              <a href=""><li class="list-group-item">Dapibus ac facilisis in</li></a>
              <a href=""><li class="list-group-item">Morbi leo risus</li></a>
              <a href=""><li class="list-group-item">Porta ac consectetur ac</li></a>
              <a href=""><li class="list-group-item">Vestibulum at eros</li></a>
                <a href=""><li class="list-group-item">Cras justo odio</li></a>
              <a href=""><li class="list-group-item">Dapibus ac facilisis in</li></a>
              <a href=""><li class="list-group-item">Morbi leo risus</li></a>
              <a href=""><li class="list-group-item">Porta ac consectetur ac</li></a>
              <a href=""><li class="list-group-item">Vestibulum at eros</li></a>    
            </ul>
        </div>
  </div>
</div>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    </body>
</html>